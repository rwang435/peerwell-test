#!/bin/bash

function usage()
{
	echo
	echo Provide useful metrics on incoming and approved videos
	echo
	echo usage
	echo "$0 kind"
	echo "  " ' - kind is one of incoming | approved'
	echo "      incoming " s3://peerwell-incoming-images
	echo "      approved " s3://peerwell-approved-videos
}

if [ "$#" != 1 ]; then
	usage
	exit
fi

if [ "$1" == "incoming" ]; then
	BUCKET="s3://peerwell-incoming-images"
elif [ "$1" == "approved" ]; then
	BUCKET="s3://peerwell-approved-videos"
elif [ "$1" == "lookup" ]; then
	echo NOT YET
	exit
else
	echo bad kind: $1
	usage
	exit
fi
echo $BUCKET


# get all videos for all emails
all_videos=$(aws s3 ls "${BUCKET}" --recursive | grep '@' | awk '{print $4}')

# For each email, show the counts of recieved kinds, videos. 
declare -a output=()
email_total=0
last_email=""
kind_count=0
last_kind=""
video_total=0
vid_count=0
vid_kind_count=0
email=""
for item in ${all_videos}; do
	#echo "${item}}"
	email=$(dirname $(dirname $item))
	kind=$(basename $(dirname $item))
	vid=$(basename $item)
	#echo "${email}, ${kind}, ${vid}, ${email_total}, ${kind_count}, ${vid_count}, ${vid_kind_count}"

	#if [ "$email" != "manish@peerwell.co" ] ; then
	#	continue
	#fi

	if [ "$email" != "$last_email" ] ; then
		email_total=$((email_total+1))
		if [ "$last_email" != "" ] ; then # not first time through
			output+=( "$(echo $last_email,$kind_count,$vid_count)" )
		fi
		last_email=$email

		last_kind=""
		kind_count=0
		vid_count=0
		vid_kind_count=0
	fi
	if [ "$kind" != "$last_kind" ] ; then
		kind_count=$((kind_count+1))
		last_kind=$kind

		vid_kind_count=0
	fi

	vid_count=$((vid_count+1))
	vid_kind_count=$((vid_kind_count+1))
	video_total=$((video_total+1))
	#echo $vid_count, $video_total

done
output+=( "$(echo $email,$kind_count,$vid_count)" ) # show the last one

#
echo "_email,kind_count,vid_count" # headers
for o in "${output[@]}"
do
   echo $o
done | sort -f
echo 
echo "totals:,emails $email_total,videos $video_total"


# People and ther video counts
echo
echo Videos per person
for item in ${all_videos}; do
	dirname $(dirname $item)
done | sort | uniq -c | sort -nr


# Top 10 people with the most videos
echo
echo Top 10 people with the most Videos
for item in ${all_videos}; do
	dirname $(dirname $item)
done | sort | uniq -c | sort -nr | head -10

# Count of videos per kind, sorted by count in decending order.
echo
echo Kinds of videos
for item in ${all_videos}; do
	basename $(dirname $item)
done | sort | uniq -c | sort -nr

# Profile of the videos themselves by extension
echo
echo videos formats
for item in ${all_videos}; do
	videofile=$(basename $item)
	extension="${videofile##*.}"
	echo $extension
done | sort | uniq -c | sort -nr

exit

