// ----------------------------------------
// label constants
//  These also control the order they are shown

// These are always relevant:
const MULTIPLE_ITEMS=0; // a bunch of things together. For single labels, this is useless
const MISORIENTED=1;  // frame rotated badly
const BLUR=2;   // blurred, unclear
const NOOP=3;   // harmless, miscellanenous, nothing to point out.
const SKIP=4;   // skip: not much differernt than earlier frames
var group_basic=[0,1,2,3,4]

// The rest are shown depending on the kind of video:
//  Group labels so that videos get a group relevant to them.

// Floor
const good_floor=10;
const bad_floor_with_clutter=11;
const bad_floor_with_damage=12;
const bad_lighting=13;
const check_floor_wires_out_of_way=14;
const check_throw_rug_is_secure=15;
var group_floor=[10,11,12,13,14,15]

// Stairs
const good_floor_railing=20; // banister surrounding the stairwell
const good_stair_railings=21; // two railings
const bad_stair_single_railing=22;
const bad_stair_no_railing=23;
const check_wall_mounted_railing=24;
const good_stair_treads=25;
const bad_stair_cluttered=26;
const bad_stair_blocked=27;
const bad_stair_tread_damage=28;
const check_carpeted_stair_is_secure=29; // derived?
var group_stair=[20,21,22,23,24,25,26,27,28,29]

// Bathroom
const good_grab_bars=30;  // or check they're secure
const good_towelrack=31;
const good_bath_mat=32;
const good_sink=33;
const good_toilet=34;
const good_tub=35;
const good_shower=36;
// these two are derived, not recognized: so skip them.
//  const bad_bathroom_without_grab_bars=0; // derived from not seeing them.
//  const bad_bathroom_without_tub_mat=1; // derived from not seeing them.
var group_bathroom=[30,31,32,33,34,35,36]

// Bedroom
const check_nightlight=40;
var group_bedroom=[40]

//
// end label constants
//
// ----------------------------------------

// For each label, this is label info used to display it as buttons or whatever.
function label_info_indexed()
{
  m = [];

  m[ MULTIPLE_ITEMS ] = {key:"M",display:"Multiple", class:"multiple", explain:"more than one item in the frame"}
  m[ MISORIENTED ] = {key:"O",display:"Orientation", class:"orientation", explain:"image is upside down or on it's side"}
  m[ BLUR ] = {key:"B",display:"Blur", class:"blur", explain:"Not recognizeable"}
  m[ NOOP ] = {key:"N",display:"Nil", class:"nil", explain:"Non-hazard, in-between image"}
  m[ SKIP ] = {key:" ",display:"Skip", class:"skip", explain:"Almost identical to the previous image"}

  /* These are derived by not being found at then end of a recognition session. They are not identified.
  m[ bad_bathroom_without_grab_bars ] =
    {key:"a",display:"br no grabbars", class:"bad_bathroom_without_grab_bars"}
  m[ bad_bathroom_without_tub_mat ] =
    {key:"a",display:"br no tub or shower mats", class:"bad_bathroom_without_tub_mat"}
  */
  m[ good_grab_bars ]                 = {key:"g",display:"grab bars", class:"good_grab_bars", explain:"grab bars are visible"}
  m[ good_bath_mat ]                  = {key:"m",display:"mat", class:"good_bath_mat", explain:"a mat is on the bathroom floor or tub"}
  m[ good_towelrack ]                 = {key:"T",display:"towelrack", class:"good_towelrack", explain:"towelrack is visible, with or without towels"}
  m[ good_sink ]                      = {key:"S",display:"sink", class:"good_sink", explain:"sink is central to the image"}
  m[ good_toilet ]                    = {key:"o",display:"toilet", class:"good_toilet", explain:"toilet is central to the image"}
  m[ good_tub ]                       = {key:"u",display:"tub", class:"good_tub", explain:"tub is central to the image"}
  m[ good_shower ]                    = {key:"R",display:"shower", class:"good_shower", explain:"shower is central to the image"}

  m[ bad_floor_with_clutter ]         = {key:"c",display:"clutter", class:"bad_floor_with_clutter", explain:"items on the floor requiring step over"}
  m[ bad_floor_with_damage ]          = {key:"d",display:"damage", class:"bad_floor_with_damage", explain:"surface is uneven or insecure"}
  m[ check_floor_wires_out_of_way ]   = {key:"w",display:"wires", class:"check_floor_wires_out_of_way", explain:"wires are central to the image"}
  m[ good_floor ]                     = {key:"f",display:"good floor", class:"good_floor", explain:"floor is clear and safe"}
  m[ check_throw_rug_is_secure ]      = {key:"r",display:"rug", class:"check_throw_rug_is_secure", explain:"a throw rug is visible and central"}

  m[ bad_lighting ]                   = {key:"l",display:"bad light", class:"bad_lighting", explain:"room is dark, or opening a door to unlit room"}

  m[ check_nightlight ]               = {key:"n",display:"nightlight", class:"check_nightlight", explain:"nightlight is visible"}

  // stairs
  m[ bad_stair_blocked ]              = {key:"b",display:"blocked", class:"bad_stair_blocked", explain:"stair has a gate"}
  m[ bad_stair_cluttered ]            = {key:"C",display:"stair clutter", class:"bad_stair_cluttered", explain:"stair is obstructed with clutter"}
  m[ bad_stair_no_railing ]           = {key:"0",display:"no railing", class:"bad_stair_no_railing", explain:"staircase walls can be seen and there is no railing"}
  m[ bad_stair_single_railing ]       = {key:"1",display:"one railing", class:"bad_stair_single_railing", explain:"staircase walls can be seen and there is only 1 railing"}
  m[ bad_stair_tread_damage ]         = {key:"D",display:"stair damage", class:"bad_stair_tread_damage", explain:"stair tread is uneven, damaged, or insecure"}
  m[ check_carpeted_stair_is_secure ] = {key:"t",display:"stair carpet", class:"check_carpeted_stair_is_secure", explain:"clear stair that is carpeted"}
  m[ check_wall_mounted_railing ]     = {key:"W",display:"wall railing", class:"check_wall_mounted_railing", explain:"when looking at one railing, it is a wall-mounted one"}
  m[ good_floor_railing ]             = {key:"a",display:"floor railing", class:"good_floor_railing", explain:"these are the railings visible at the top of stairs"}
  m[ good_stair_railings ]            = {key:"2",display:"railing", class:"good_stair_railings", explain:"staircase walls can be seen and there are two railing"}
  m[ good_stair_treads ]              = {key:"s",display:"steps", class:"good_stair_treads", explain:"stair treads are central and they are hazard free"}

  return m;
}

// --------------------

const label_index = label_info_indexed()

function get_labels_from_idx( idx )
{
  var info = label_index[idx]
  if( typeof info == "undefined" )
	console.log( "FATAL: undefined label idx ",idx)
  return label_index[idx]
}

function get_labels_from_kind( kind )
{
  switch( kind ) {
  // FLOOR
    case "wires":
    case "clutter":
    case "rugs":
    case "damaged":
    case "halls":
    case "surfaces":
      console.log( "floor labels" )
      return group_basic.concat(group_floor)
  // STAIRS
    case "full-stairs":
    case "up-stairs":
    case "down-stairs":
    case "banisters":
    case "stairs-clutter":
    case "stairs-gate":
    case "stairs-lift":
      console.log( "stair labels" )
      return group_basic.concat(group_stair)
  // BEDROOM
    case "bed-floor-scan":
    case "bed-no-lights":
    case "nightlights-dark":
    case "nightlights-light":
      console.log( "bedroom labels" )
      return group_basic.concat(group_floor.concat(group_bedroom))
  // BATHROOM
    case "toilet":
    case "sink":
    case "towelrack-towels":
    case "towelrack-no-towels":
    case "bathroom-floor":
    case "tub":
    case "grabbars":
    case "bathroom-no-lights":
      console.log( "bathroom labels" )
      return group_basic.concat(group_floor.concat(group_bathroom))
  // ALL
    case "bed-bath-path":
    case "ANY":
    default:
      console.log( "defaulting to all labels" )
      return group_basic.concat( group_floor.concat( group_stair.concat( group_bathroom.concat( group_bedroom))))
  }
}


// --------------------------------

// given a list of labels, create elements and them to the target id.
function install_labels( labels, target_id )
{

  var e = document.getElementById( target_id );
  if( typeof e == "undefined" )
    return false

  e.innerHTML = ""
  for( i=0, max=labels.length;i < max; i++ ) {
    //console.log( i )
    label_id=labels[i]
    label_info = get_labels_from_idx(label_id)
    label_str = label_info.display + " ("+label_info.key+")"
    //e.innerHTML += '<input type="submit" name="result" value="'+label_info.class+'" data-key="'+label_info.key+'">'+label_str+'</input>'
    e.innerHTML += '<button type="submit" name="result" value="'+label_info.class+'" data-key="'+label_info.key+'" title="'+label_info.explain+'">'+label_str+'</button>'
  }

  return true
}

// --------------------------------

var e=document.getElementById( "kind" )
if( typeof e == "undefined" )
  console.log( "FATAL: var kind must be defined with a valid video category by being present in the supplied csv" )
else {
  var kind=e.getAttribute("value")
  labels = get_labels_from_kind( kind )
  target_id = "choices"
  if( !install_labels( labels, target_id ) )
    console.log( "FATAL: html is missing an element with id " + target_id )
}
