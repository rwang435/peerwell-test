
# Labling images...
The goal is to:

- review all the images by at least 3 people and take those that have a majority.

 - It may be ok, to have only one person do them.
 - Or at least have all the images labeled at least once.

Secondarily: 

- It would be nice if the images were stored on S3 or some shared area and not downloaded.

## Solutions

### Localturk
Tried this:

```
nvm use v6.1.1
npm install -g localturk
classify-images --labels 'Has a red ball,Does not have a red ball' sample/images/*.png
localturk -h
```
This classifies images on a single machine. It seems to work only from local images, so I'd have to download frames. The results are saved locally in a csv, as they are from mturk.

  - Need S3 downloader for only those images not yet classsified or not yet at majority

 - I'd have to upload the csvs, and merge them in order to accumulate a sum. This will take work since.
   - When a user starts to the labeling process, this accumulated csv results would be consulted, the files would be 'checked out', while the classification takes place

