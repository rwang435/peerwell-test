#!/bin/bash

# 1. Requires the right version of node and npm 
source  ~/.nvm/nvm.sh
nvm use 'v6.11.0'
ERR=$?
if [ "$ERR" != "0" ] ; then
	ERRSTR="nvm use failed - error code : $ERR"
	echo "${ERRSTR}"
	exit
fi

function install_localturk() {
	# Requires global install of localturk
	npm install -g localturk
}

# 3. Check args
usage() {
	echo 
	echo Image Labeling
	echo 
	echo Usage: $0 csv_in_path csv_out_path
	echo "	csv_in_path  - relative path to a csv with rows of frame urls to be labeled paired with the video kinds the frames are from."
	echo "	csv_out_path - relative path to the output csv, same as above, but with a column for the label."
  echo
	echo "	The paths should be similar to this:"
  echo "      tmp/_labels/in/VIDEO_NAME.csv"
  echo "      tmp/_labels/out/VIDEO_NAME.csv"
  echo
	echo "Misc:"
	echo Usage: $0 install localturk
	echo "	Use this install localturk in the proper NODE & NPM environment"
	#echo "  get_batch - to label. Either a random batch, by video_label, by a prior label"
	#echo "  return_batch - merge labeling results"
	#echo "  get_labeled_frames - using the label list, get frames that match the supplied labels or all labels, and with one of: >=n votes, or <= n votes"
	
	exit
}
if [ "$#" != "2" ] ; then
	echo missing arguments
	usage
elif [ "$1" == "install" -a "$2" == "localturk" ] ; then
	install_localturk
	exit
fi

csv_in_path=$1
csv_out_path=$2

## parse the path for the kind of video this is supposed to bse
#kind_dir=$(dirname $(dirname "$csv_in_path") )
#in=$(basename $(dirname "$csv_in_path") )
#if [ "$in" != "in" -o "$kind_dir" == "" ] ; then
#	echo bad csv url: $csv_in_path
#	echo expecting something like: _labels/clutter/in/df04155fac9eb6aa63e3744b79487a3d.csv
#	usage
#fi
#results_dir="$kind_dir/out"
#kind=$(basename $kind_dir)
#mkdir -p $results_dir

# help
#localturk -h
#Usage: localturk [options] template.html tasks.csv outputs.csv
# -V, --version           output the version number
# -p, --port <n>          Run on this port (default 4321)
# -s, --static-dir <dir>  Serve static content from this directory. Default is same directory as template file.
# -r, --random-order      Serve images in random order, rather than sequentially. This is useful for generating valid subsamples or for minimizing collisions during group localturking.
# -w, --write-template    Generate a stub template file based on the input CSV.
# -h, --help              output usage information
# Create an html template to work with
#localturk --write-template tasks.csv > template.html
#localturk --write-template $1 > template.html

localturk labeling/label.html "$csv_in_path" "$csv_out_path"


# image classification
# classify-images --help
# Usage: classify-images [options] /path/to/images/*.jpg
# -V, --version             output the version number
# -o, --output <file>       Path to output CSV file (default output.csv) (default: output.csv)
# -l, --labels <csv>        Comma-separated list of choices of labels (default: Yes,No)
# -w, --max_width <pixels>  Make the images this width when displaying in-browser
# -h, --help                output usage information

#imgs=$(tail -n +2 $1)
#rm out.csv
#classify-images -o out.csv --max_width 512 --labels 'apple,tomato,pear,onion,salt,pepper' $imgs
