#!/bin/bash
#

# -------------------------------
# config

TRAINING_BUCKET=s3://peerwell-hazard-ml
TRAINING_BUCKET_PATH=$TRAINING_BUCKET/images
TMP_DIR=tmp

BASELINE_SEARCH=~/ml_data/baseline_search

APP_UPLOAD_BUCKET=s3://peerwell-ml-app-upload

# -------------------------------

usage() {
	if [ "$1" != "help" ] ; then
		# basic usage
		echo
		echo "Usage:"
		echo "  $0 command training_set_directory [optional args ...]"
		echo "command is one of:"
		echo "  help"
		echo "  list, restore, save"
		echo "    restore_all, save_all"
		echo "    list_zip, restore_zip, save_zip"
		echo "  prep, merge, reorg, rando, cull,"
		echo "    dupes, uniq,"
		echo "    label, label_resume,"
		echo "  train, validate"
		echo "  get_uploads, template"
		echo
		exit
	fi
	# extended help
	echo
	echo "GeneralDescription:"
	echo "  This script does three major things:"
	echo "    1. Manages training sets to be used in training and validating models."
	echo "    2. Trains a model with the given training set."
	echo "    3. Validates a model against a given validation set."
	echo
	echo "  When it comes to managing training sets, the primary activitiy is"
	echo "    to 'prepare' a training set. This involves merging a number of"
	echo "    smaller, component training sets and normalizing their labels for training."
	echo "    The training sets are assumed to be local to your machine. You:"
	echo "      - get the latest baseline_search and baseline_uploaded training sets via 'list' and 'restore'"
	echo "      - get the latest labeled video frames training set via 'labeling/imagectl.sh download_frames'"
	echo "    then you merge and clean them up via:"
	echo "      $0 prep training_set_directory validation_set_dir video_set baseline_uploaded"
	echo "    For example:"
	echo "      labeling/train.sh prep ~/ml_data/hazards_20180403 ~/ml_data/validate_20180403 tmp/labeled_frames ~/ml_data/baseline_uploaded"
	echo "    will create a new training and validation set in '~/ml_data/' as hazards_20180403 and ~/ml_data/validate_20180403 respectively"
	echo "      using the video frames you downloaded, the set that resulted from processing the app uploaded images, and the baseline_search set."
	echo "      (NB: the baseline_search set is loaded automatically but must be present on your machine.)"
	echo
	echo "  You also can:"
	echo "    'list', 'restore' training sets saved on s3."
	echo "    'save' a training set in ~/ml_data to s3."
	echo "    modify a training set as follows:"
	echo "      - remove duplicated images across the training set"
	echo "      - initiate a review of the training set"
	echo "      - merge with other training sets"
	echo "      - reorganize a training set by applying a formula that groups and removes labels and the corresponding images"
	echo "      - extract a validation set from the larger training set"
	echo "  NB: training sets on s3 are a shared resource so saving can crush a shared set such as the baseline sets."
	echo "-------------------------------------------"
	echo
	echo "General Usage:"
	echo
	echo "  $0 command training_set_directory [optional args]"
	echo
	echo "  'command' is one of the commands below..."
	echo "  'training_set_directory' is the location of a training set. There are"
	echo "       special names and locations for these... (see below)"
	echo
	echo "  Run a command without arguments to get more help on it."
	echo
	echo "-------------------------------------------"
	echo "COMMANDS:"
	echo
	echo "Automatic preparations before Training:"
	echo "  prep            - does all the manual steps listed below to generate a new training set for training. In"
	echo "        	          particular, after you download our labeled videos, and corrected the app uploaded"
	echo "                    image labels and added those to the baseline_uploaded set, you are ready to go."
	echo "                    Then run this command and it will: merge both of those sets with the Google-derived"
	echo "                    training set I handcrafted. The output training set will be reorg'd, cull'd, and"
	echo "                    then rando'd to get a validation set as well."
	echo "                    Then train with the output training set and validate with the validation set."
	echo
	echo "Training:"
	echo "  train           - train on the provided training set."
	echo "  validate        - run the recognizer on the validation set and see how it does."
	echo
	echo "  NB: see ./tensorflow-for-poets for the supporting scripts: 'train.sh', 'recognize.sh' and 'scripts' directory"
	echo
	echo "----------"
	echo
	echo "Finding & Getting the Training Set:"
	echo "  list            - list all the saved training sets on s3"
	echo "  restore         - download named training set from s3 to ~/ml_data"
	echo "  save            - upload one training sets in ~/ml_data to s3"
	echo "  restore_all     - download all the saved training set from s3 to ~/ml_data"
	echo "  save_all        - upload all the training sets in ~/ml_data to s3"
	echo "  These are an older form:"
	echo "    list_zip      - list the saved training sets zipped on s3"
	echo "    restore_zip   - download and install a zipped training set from s3"
	echo "    save_zip      - save the training set as a zip on s3"
	echo
	echo "  Special Locations for Training Sets:"
	echo "    The default location training sets is \`~/ml_data/\` as it not backed up by Apple iTunes drive"
	echo "      The AWS bucket that stores this is '${TRAINING_BUCKET}'. Copy that to your ~/ml_data directory (or sync with it)"
	echo "    The location of the downloaded training set from our Training Set Pipeline is \`tmp/labeled_frames\`"
	echo
	echo "  Special Training Sets:"
	echo "    baseline_search          - Baseline training set of 1660 labeled and uniquely id'd images obtained through search."
	echo "    baseline_uploaded        - Baseline training set of curated images obtained from app uploads."
	echo
	echo "Info about a training set:"
	echo "  stats           - given a training set directory, provides stats on it."
	echo
	echo "----------"
	echo
	echo "Manual preparations before Training:"
	echo "  merge           - the first listed training directory is created if need be, and then the remaining directories, "
	echo "                     up to 3, are copied into it, in order,"
	echo "  reorg           - reorganize the training set. Combines and deletes the labeled images in special ways"
	echo "  cull            - remove any label that has fewer than 62 images in it. Training needs more."
	echo "  cull_count      - like cull, but only identifies labels are too few but does NOT remove them."
	echo "  rando           - move 2 random files from each label in the training set to the validation set. (last argument)"
	echo
	echo "----------"
	echo
	echo "App Uploads Management and Manual Classification:"
	echo "  get_uploads     - download from S3 user uploaded images that are newer than the last download."
	echo "  template        - create a directory and non-destructively create subdirectories for all the labels a proper training set should have."
	echo "  demerge         - removes files from the given training set that are present in a 'source' training set."
	echo
	echo "  NB: These are used to get latest uploaded images, and a clean template directory to classify the uploaded images into."
	echo "      The classified directory can then be merged into the baseline_uploaded training set, and saved for use."
	echo
	echo "----------"
	echo
	echo "Special Commands for Search-derived Training Sets:"
	echo "  The training set I created from Google Images requires modifications to work with our newer Image Labling Pipeline."
	echo "  The following two commands were used on this set to create the 'baseline_search' training set."
	echo "  dupes           - check for duplicates by name."
	echo "  uniq            - change the names of files in the training directory so that they're unique"
	echo "                    Required ONLY on directories of labels I got from Google."
	echo "                    DO NOT USE on: the downloaded training set from our Pipeline, nor on"
	echo "                    the app uploaded training sets as they are already unique, and because"
	echo "                    it will mangle the already meaningful names."
	echo "  NB: the baseline_search is ready to use. The duping & uniqing have been done."
	echo
	echo "----------"
	echo
	echo "QA the Training Set: (not fully done yet)"
	echo "  For grooming the training set (i.e. manually 'turking the training set."
	echo "  label           - confirm the labels of the images in training_dir"
	echo "  label_resume    - resume confirming the labels of the images in training_dir"
	echo

	exit
}


# -------------------------------
# basic arg check

if [ "$#" -lt 1 ] ; then
	echo 
	echo "missing args: $# args provided"
	echo 
	usage
fi


# -------------------------------
# utility functions

training_list=""
function list_all_training_files() {
	dir=$1
	if [ "$dir" == "" ] ; then
		echo "Need valid training directory e.g. ~/ml_data/hazards"
		exit
	fi

	training_list=$(find "${dir}" -type f)
}

#	argcheck_training_dir $2
function argcheck_training_dir() {
	if [ "$1" == "" ] ; then
		echo missing local training_dir argument
		usage
	fi
}

# check if 2nd arg is present, 1st argument is the help
function check_arg() {
	if [ "$2" == "" ] ; then
		echo missing argument: $1
		usage
	fi
}

# Paranoid version of directory removal: rather than rm -r, which
#	can lead to removing of unintended directories if mis-called.
# Will remove hidden junk, but fail otherwise if the directory non-empty.
function rm_dir() {
	# remove hidden junk first
	for i in $* ; do
		if [ ! -d "$i" ]; then
			# dir doesn't exist skip...
			continue
		fi
		# remove any remaining files in the dir, even those prefixed with .
		shopt -s dotglob # set . expansion to catch files prefixed with . e.g. .DS_Store
		rm $i/*
		shopt -u dotglob # unset . expansion 
		# now try to remove the directory
		rmdir $i
		ERR=$?
		if [ "$ERR" != "0" ] ; then
			echo "err $ERR - failed: 'rmdir $i'"
			exit
		else
			echo removed $i
		fi
	done
}

# SEE: https://docs.google.com/spreadsheets/d/1viQfhXZoiqMkPV6ok6VHWZD94hZgdpuL4XvKtdgUTAU/edit?usp=sharing

# This reorganizes the training set in ways we hope lead to
#	better recognition and meet our detection needs.
function reorg1() {

	# These are untouched...
	#
	#	check_carpeted_stair_is_secure
	#	check_floor_wires_out_of_way
	#	check_nightlight
	#	check_throw_rug_is_secure
	#	check_wall_mounted_railing
	#	
	#	good_bath_mat
	#	good_floor
	#	good_grab_bars
	#	
	#	blur
	#	nil

	# Other labels are combined into these:
	#
	# good_stairs
	# good_bathroom
	# bad_floor
	# bad_stair_railing
	# bad_stairs_blocked

	# Remove labeling artifacts
	rm -rf multiple
	rm -rf orientation
	rm -rf skip

	# Remove out of date labeling ideas
	rm -rf bad_bathroom_without*

	# Remove labels we don't have enough of or are not helping...
	rm -rf bad_lighting # This should be tested

	# group the remaining labels together as needed...

	mkdir -p good_stairs
	mv good_floor_railing/* good_stairs
	mv good_stair_railings/* good_stairs
	mv good_stair_treads/* good_stairs
	rm_dir good_stair_treads good_floor_railing good_stair_railings

	mkdir -p good_bathroom
	mv good_shower/* good_bathroom
	mv good_sink/* good_bathroom
	mv good_toiler/* good_bathroom
	mv good_toilet/* good_bathroom
	mv good_towelrack/* good_bathroom
	mv good_tub/* good_bathroom
	rm_dir good_sink good_towelrack good_toilet good_toiler good_tub good_shower

	mkdir -p bad_floor
	mv bad_floor_with_clutter/* bad_floor
	mv bad_floor_with_damage/* bad_floor
	rm_dir bad_floor_with_damage bad_floor_with_clutter

	mkdir -p bad_stair_railing
	mv bad_stair_single_railing/* bad_stair_railing
	mv bad_stair_no_railing/* bad_stair_railing
	rm_dir bad_stair_single_railing bad_stair_no_railing

	mkdir -p bad_stairs_blocked
	mv bad_stair_blocked/* bad_stairs_blocked
	mv bad_stair_cluttered/* bad_stairs_blocked
	mv bad_stair_tread_damage/* bad_stairs_blocked
	rm_dir bad_stair_blocked bad_stair_cluttered bad_stair_tread_damage

}

# meta2
function reorg2() {

	reorg1

	# nil
	mkdir -p nil
	mv good_stairs/* nil
	mv good_bathroom/* nil
	rm_dir good_stairs good_bathroom

	mv blur/* nil
	rm_dir blur

	# Good things
}

# -----------------------------
if [ "$1" == "stats" ] ; then
	echo stats

	count=0
	list_all_training_files $2
	for i in $training_list ; do
		label=$(basename $(dirname $i))
		echo $label
		count=$((count+1))
	done > $TMP_DIR/train_labels
	echo $count labeled images in $2

	# counts of label categories
	cat $TMP_DIR/train_labels | sort | uniq -c | sort -nr > $TMP_DIR/train_labels_counts
	cat $TMP_DIR/train_labels_counts

# -----------------------------
elif [ "$1" == "label" -o "$1" == "label_resume" ] ; then

	argcheck_training_dir $2
	training_dir=$2

	# This requires that you install into chrome this extension
	#		https://chrome.google.com/webstore/detail/web-server-for-chrome/ofhbbkphhbklhfoeikjpcbhemlocgigb
	# and it must configured to
	#		- show training_dir
	#		- serve from http://127.0.0.1:8887
	#		- serve directory ~/ml_data

	# csv list each image for use in turk/localturk
	# --------------------
	TRAINING_INPUT_CSV="$TMP_DIR/train_labels_in.csv"
	TRAINING_OUTPUT_CSV="$TMP_DIR/train_labels_out.csv"
	HTTPSERVER="http://127.0.0.1:8887"
	DOCROOT=$(basename $training_dir)

	if [ "$1" != "label_resume" ] ; then
		echo removing prior stuff
		rm $TRAINING_OUTPUT_CSV
	fi

	list_all_training_files "${training_dir}"
	count=0
	echo "image_url,kind,result" > "${TRAINING_INPUT_CSV}"
	for i in $training_list ; do
		filename=$(basename $i)
		label=$(basename $(dirname $i))

		echo "${HTTPSERVER}/${DOCROOT}/${label}/${filename},$label," # csv format
		count=$((count+1))
	done >> "${TRAINING_INPUT_CSV}"
	echo $count
	#cat "${TRAINING_INPUT_CSV}"

	labeling/label_batch.sh "${TRAINING_INPUT_CSV}" "${TRAINING_OUTPUT_CSV}"

# -----------------------------
elif [ "$1" == "list" ] ; then
	echo list
	# NB: need the ending '/' on the s3 path for this to work!!
	aws s3 ls s3://peerwell-hazard-ml/ml_data/ | grep 'PRE' | awk '{print $2}'

elif [ "$1" == "save_all" ] ; then
	echo save_all
	aws s3 sync ~/ml_data ${TRAINING_BUCKET}/ml_data

elif [ "$1" == "restore_all" ] ; then
	echo restore_all
	aws s3 sync ${TRAINING_BUCKET}/ml_data ~/ml_data

elif [ "$1" == "save" ] ; then
	echo save
	check_arg "name of training set dir in ~/ml_data" $2
	echo removing ${TRAINING_BUCKET}/ml_data/$2 in 10 seconds
	sleep 10
	aws s3 rm ${TRAINING_BUCKET}/ml_data/$2 --recursive
	aws s3 sync ~/ml_data/$2 ${TRAINING_BUCKET}/ml_data/$2

elif [ "$1" == "restore" ] ; then
	echo restore
	check_arg "name of training set dir in ~/ml_data" $2
	aws s3 sync ${TRAINING_BUCKET}/ml_data/$2 ~/ml_data/$2


# -----------------------------
# old
elif [ "$1" == "list_zip" ] ; then
	echo list zipped training sets
	aws s3 ls ${TRAINING_BUCKET_PATH}/ --human-readable

# -----------------------------
# old
elif [ "$1" == "save_zip" ] ; then
	echo save training set zipped

	argcheck_training_dir $2
	training_dir=$2

	# Do the zip from within the saved directory so that paths are local to it.
	#	On restore, similary, go the target directory and restore from there.
	pushd .
	cd $training_dir

	# zip the directory
	#DATED=`date "+%Y-%m-%d-%H-%M-%S"`
	DATED=`date "+%Y-%m-%d-%H-%M"`
	ARCHIVE_FILE=labeled_images-${DATED}.zip
	zip -r ${training_dir} ${training_dir}
	mv $training_dir.zip ${ARCHIVE_FILE}

	# copy to s3
	#		aws s3 mb ${TRAINING_BUCKET}
	aws s3 cp $ARCHIVE_FILE ${TRAINING_BUCKET_PATH}/${ARCHIVE_FILE}
	ERR=$?
	echo err $ERR

	rm ${ARCHIVE_FILE}

	popd

# -----------------------------
# old
elif [ "$1" == "restore_zip" -a "$3" != "" ] ; then
	echo restore training set from zip

	argcheck_training_dir $2
	training_dir=$2

	# Get and check the zip file name provided.
	filename=$(basename $3)
	dirs=$(dirname $3)
	nozip=$(basename "$filename" '.zip')
	if [ "$dirs" != "" -a "$dirs" != "." ] ; then
		echo bad arg: $3
		echo provide just the zip filename
		usage
	elif [ "$filename" == "$nozip" ] ; then
		echo $3 not a proper zip name.
		usage
	fi

	#
	# go to the training dir and copy down the zip file and unpack it:
	#

	pushd .
	mkdir -p "${training_dir}"
	cd "${training_dir}"
	pwd

	# copy from the bucket to tmp file, then unzip
	aws s3 cp "${TRAINING_BUCKET_PATH}/${filename}" tmp.zip
	ERR=$?
	if [ "$ERR" != "0" ] ; then
		echo err $ERR
		popd
		exit
	fi

	unzip tmp.zip
	echo "This restore to a subdirectory which you will need to move up... will fix eventually"

	rm tmp.zip

	popd


# -----------------------------
elif [ "$1" == "merge" ] ; then
	echo merge

	if [ "$2" == "" -o "$3" == "" ] ; then
		echo "missing args: $0 merge target_training_dir training_set_dir_1 [training_set_dir_2]"
		echo " target_training_dir will be created if it does not exist."
		usage
	fi

	argcheck_training_dir $2
	training_dir=$2

	echo merging to $training_dir
	mkdir -p $training_dir


	if [ "$3" != "" ] ; then
		pushd .
		echo "1. copying $3"
		cd "$3"
		cp -Rp * $training_dir
		popd
	fi

	if [ "$4" != "" ] ; then
		echo "2. copying $4"
		pushd .
		cd "$4"
		cp -Rp * $training_dir
		popd
	fi

	if [ "$5" != "" ] ; then
		echo "3. copying $5"
		pushd .
		cd "$5"
		cp -Rp * $training_dir
		popd
	fi

# -----------------------------
elif [ "$1" == "dupes" ] ; then
	echo dupes check

	argcheck_training_dir $2
	training_dir=$2
	find "${training_dir}" "-type" f > $TMP_DIR/tmp.all

	training_list=$(cat $TMP_DIR/tmp.all)
	for i in $training_list ; do
		filename=$(basename $i)
		count=$(grep "${filename}" "$TMP_DIR/tmp.all" | wc -l)
		if [ "$count" -gt 1 ] ; then
			echo ${filename} duplicated in $(grep "${filename}" "$TMP_DIR/tmp.all")
		fi
	done

# -----------------------------
elif [ "$1" == "reorg" ] ; then
	echo reorg

	argcheck_training_dir $2
	training_dir=$2

	pushd .
	cd $training_dir

	reorg1
	#reorg2

	popd

# -----------------------------
elif [ "$1" == "uniq" ] ; then
	echo uniq

	count=0
	list_all_training_files $2
	for i in $training_list ; do
		dir=$(dirname $i)
		filename=$(basename $i)
		extension="${filename##*.}"
		DATED=`date "+%Y-%m-%d-%H-%M-%S"`
		mv $i $dir/${DATED}_${count}_$filename

		count=$((count+1))

	done

# -----------------------------
elif [ "$1" == "rando" ] ; then
	echo rando

	argcheck_training_dir $2
	dir_src=$2

	check_arg "target validation directory" $3
	#dir_tgt=~/ml_data/validate
	dir_tgt=$3

	N=2 # number of random files per directory

	echo $N files will be selected from the training set $dir_src and moved to $dir_tgt
	echo in 10 seconds
	sleep 10

	dirs=$(find $dir_src/* -type d)
	for dir in $dirs ; do
		random_files=$(find $dir -type f | sort -R | tail -$N )
		for f in $random_files ; do
			#echo $f
			filename=$(basename $f)
			label=$(basename $(dirname $f))
			mkdir -p $dir_tgt/$label
			# move it out the training set
			mv $f $dir_tgt/$label/$filename 
		done
	done

# -----------------------------
elif [ "$1" == "cull" -o "$1" == "cull_count" ] ; then
	echo cull # remove any directory with fewer than 62 images

	argcheck_training_dir $2
	training_dir=$2

	dir_src=$training_dir

	dirs=$(find $dir_src/* -type d)
	for dir in $dirs ; do
		count=$(find $dir -type f | wc -l)
		ERR=$?
		if [ "$ERR" != "0" ] ; then
			echo Err $ERR : counting files in $dir
			exit
		fi

		echo $dir has $count files
		if [ $count -lt 62 ] ; then
			if [ "$1" == "cull_count" ] ; then
				echo ' => ' $dir has too few files
			else
				echo removing $dir
				rm -rf $dir
			fi
		fi
	done

# -----------------------------
elif [ "$1" == "prep" ] ; then
	echo prep
	baseline_set=$BASELINE_SEARCH

	if [ "$2" == "" -o "$3" == "" -o "$4" == "" ] ; then
		echo missing args: $0 prep training_dir_to_be_created validation_dir_to_be_created labeled_video_dir labeled_upload_dir
		echo automatically merge in $baseline_set
		usage
	elif [ "$2" == "$3" -o "$2" == "$4" -o "$3" == "$4" ] ; then
		echo dirs must be different
		usage
	fi

	argcheck_training_dir $2
	training_dir=$2

	check_arg "target validation directory" $3
	validation_dir=$3

	check_arg "video training set" $4
	video_set=$4

	upload_set=$5


	$0 merge $training_dir $BASELINE_SEARCH $video_set $upload_set
	$0 reorg $training_dir
	# cull before rando!
	$0 cull $training_dir
	$0 rando $training_dir $validation_dir
	#$0 save $training_dir

# -----------------------------
elif [ "$1" == "train" ] ; then
	echo train # remove any directory with fewer than 60 images

	argcheck_training_dir $2
	training_dir=$2

	pushd .
	cd tensorflow-for-poets-2
	source ~/tensorflow/bin/activate
	./train_10.sh $training_dir
	popd

# -----------------------------
elif [ "$1" == "validate" ] ; then
	echo validate # remove any directory with fewer than 60 images

	if [ "$2" == "" -o "$3" == "" ] ; then
		echo missing args: $0 validate training_dir date_id_of_generated_model
		usage
	fi

	argcheck_training_dir $2
	validation_dir=$2

	check_arg "date_id emitted during training" $3
	date_id=$3

	pushd .
	cd tensorflow-for-poets-2
	source ~/tensorflow/bin/activate
	./recognize.sh $validation_dir $date_id
	popd

# -----------------------------
elif [ "$1" == "template" ] ; then
	echo template

	argcheck_training_dir $2
	training_dir=$2

	pushd .

	cd $training_dir
	ERR=$?
	if [ "$ERR" != "0" ] ; then
		echo err $err: directory $training_dir does not exist
		usage
	fi

	list=" bad_floor_with_clutter bad_floor_with_damage bad_lighting bad_stair_blocked bad_stair_cluttered bad_stair_no_railing bad_stair_single_railing bad_stair_tread_damage blur check_carpeted_stair_is_secure check_floor_wires_out_of_way check_nightlight check_throw_rug_is_secure check_wall_mounted_railing good_bath_mat good_floor good_floor_railing good_grab_bars good_shower good_sink good_stair_railings good_stair_treads good_toilet good_towelrack good_tub multiple nil orientation skip"
	for i in $list ; do
		mkdir -p $i
	done

	popd

# -----------------------------
elif [ "$1" == "get_uploads" ] ; then
	echo uploaded

	argcheck_training_dir $2
	training_dir=$2

	# AWS doen't seem to supporting getting files based on a date.
	#	Hence, the following hacks..

	# Set timestamp before pulling data, just so we don't miss anything during download.
	#		Remove files before the timestamp file, then rest the timestamp.
	timestamp=$training_dir/timestamp
	timestamp_new=$training_dir/timestamp.new
	echo $(date) > $timestamp_new

	#aws s3 ls $APP_UPLOAD_BUCKET
	aws s3 cp $APP_UPLOAD_BUCKET $training_dir --recursive

	#
	if [ -e $timestamp ] ; then
		echo removing files older than $(cat $timestamp)
		find $training_dir -type f ! -newer $timestamp -exec rm {} \;
	fi
	mv $timestamp_new $timestamp
	echo Set time is $(cat $timestamp)

	# TODO: Should keep the json files that are actually in use somewhere


# -----------------------------
elif [ "$1" == "demerge" ] ; then
	echo demerge

	# REMOVE file in src_dir
	argcheck_training_dir $2
	training_dir=$2

	check_arg "source dir (not removed)" $3
	src_dir=$3

	tmpfile=$TMP_DIR/_common_dirs
	# find those from src_dir present in training dir
	diff -srq $training_dir $src_dir | grep identical | awk '{print $2}' > "$tmpfile"
	echo $(wc -l "$tmpfile") in common

	# read the list and remove one-by-one.
	#		include the leading whitespace (IFS), and the last line even if it doesn't end with \n (-n $line)
	count=0
	while IFS='' read -r line || [[ -n "$line" ]]; do
    rm "$line"
		count=$((count+1))
		printf "  ${count}\r"
	done < "$tmpfile"

	rm "$tmpfile"

# -----------------------------
elif [ "$1" == "help" ] ; then
	usage help

else
	echo bad arg
	usage

fi
