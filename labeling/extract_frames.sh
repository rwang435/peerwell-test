#!/bin/bash
#

function usage()
{
	echo
	echo Video Frame Extraction
	echo
	echo usage
	echo "$0 copy_down s3_video_url"
	echo "$0 extract s3_video_url nth_frame [[rot_flip] [rot_flip]]"
	echo "$0 copy_up s3_video_url"
	echo "  1. copy_down - copy from an s3_video_url to local dir for frame extraction"
	echo "  2. extract   - extract from copied down s3_video_url every nth_frame.  Optional rotation & flip arugments"
	echo "  3. copy_up   - copy frames to proper place with respect to s3_video_url"
	echo 
	echo "   This is a three step process to be done to extract frames from a given s3_video_url."
	echo "   Extraction allows you to rotate and flip the frames."
	echo "   For help with extract\'s arguments and key commands, run: extract_frames.sh "
	echo 
	echo "   If you know the proper rotate & flip arguments, you can supply them in advance."
	echo 
	echo "$0 view s3_video_url"
  echo " view - display a video. You can learn here the proper rotation and flip arguments for extraction"
	echo 
	echo "$0 get_list s3_video_url"
	echo "  get_list  - obtain a list of the extracted frames from a given video s3_video_url"
	echo 

	exit -1
}

FRAMES_BUCKET_CLI=s3://peerwell-ml-frames # for use by the aws library and cli
LOCAL_INPUT_VIDEO_DIR="tmp/_video_in" # stores videos for frame extraction - input
LOCAL_OUTPUT_FRAMES_DIR="tmp/_frames_out" # stores frames extracted from videos - output
NFRAMES=6 # get every 6th frame

# Obtain all the parameters based on the s3_video_url
if [ "$2" == "" ] ; then
	echo need s3_video_url
	usage
fi
s3_src_url=$2

#	The s3 URL is structured: extract pieces to name things properly.
# -- get the pieces:
url=$s3_src_url
source labeling/common.sh
# -- Use pieces:
input_video_path="$LOCAL_INPUT_VIDEO_DIR/${filename}"
#output_frames_dir="$LOCAL_OUTPUT_FRAMES_DIR/$kind/${filename_without_extension}"
output_frames_dir="$LOCAL_OUTPUT_FRAMES_DIR/${filename_without_extension}"
video_name=$filename_without_extension
# -- validate
if [ "$input_video_path" == "" -o "$video_name" == "" -o "$output_frames_dir" == "" -o "$kind" == "" ] ; then
	echo bad s3 video path
	echo in $input_video_path name $video_name out_dir $output_frames_dir kind $kind
	usage
fi


# Now do the command...
ERR=0
#if [ "$1" == "start" ] ; then
#	echo start the process of extracting frames from video $s3_src_url
#	usage
#elif [ "$1" == "check" ] ; then
#	echo
if [ "$1" == "copy_down" ] ; then
  echo copy_down

  mkdir -p "$LOCAL_INPUT_VIDEO_DIR"
  mkdir -p "$output_frames_dir"
  aws s3 cp "${s3_src_url}" "${input_video_path}"
  ERR=$?

elif [ "$1" == "get_local_video_path" ] ; then
  echo "${input_video_path}" # valid once the video is copied down

elif [ "$1" == "view" ] ; then
	# DO NOT echo ANYTHING HERE since the results of the python script are parsed!!

	# run the python script:
	#		python imagebin/video-displayer.py video_path nth_frame [rot_flip_1 [rot_flip_2]]
	source ~/tensorflow/bin/activate
	python imagebin/video-displayer.py ${input_video_path} $3 $4
  ERR=$?

elif [ "$1" == "extract" ] ; then
	echo extracting video frames
	echo
	echo "1. removing prior local frames"
	rm -r "${output_frames_dir}"

	# run the python script:
	#		python imagebin/video-slicer.py video_path video_name output_dir nth_frame [rot_flip_1 [rot_flip_2]]
	echo "2. extracting: every $NFRAMES frames, with flip_rotation args: $3 $4"
	source ~/tensorflow/bin/activate
	python imagebin/video-slicer.py ${input_video_path} ${video_name} ${output_frames_dir} $NFRAMES $3 $4
  ERR=$?

	#echo 3. If the conversion is correct then run:
	#echo "    " $0 copy_up ${s3_src_url}
	#echo otherwise add some flip or rotate arguments at end like
  #echo "    " $0 extract ${input_video_path} ${video_name} ${output_frames_dir} $NFRAMES  right

elif [ "$1" == "copy_up" ] ; then
  echo copy frames up
  # aws mb $FRAMES_BUCKET_CLI
  aws s3 mv "${output_frames_dir}" "${FRAMES_BUCKET_CLI}/$video_name" --recursive
	ERR=$?
	if [ "$ERR" == "0" ] ; then
		echo removing local copy of video and extracted frames
		echo rm -r "${input_video_path}"
		echo rm -r "${output_frames_dir}"
		rm -r "${input_video_path}"
		rm -r "${output_frames_dir}"
		echo extraction complete
	else
		echo failed - aws error $ERR
	fi

elif [ "$1" == "get_list" ] ; then
	list=$(aws s3 ls "${FRAMES_BUCKET_CLI}/$video_name" --recursive | awk '{print $4}')
  echo $list

else
	echo invalid argument: $1
	usage
fi

if [ "$ERR" != "0" ] ; then
  echo error $ERR
	exit $ERR
fi

exit

