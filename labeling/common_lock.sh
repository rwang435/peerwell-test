#
# Locking
#
#
# A simple but lock scheme is implemented to keep clients from stepping on each
# other. It's not super robust, but hopefully adequate for now.
#

# unlock( s3_lock_path, local_lock_path )
# unlock
#     Simply remove the lock file, OR 
#     copy lock down and see if it's mine, and if it is remove remotely
#     and locally, otherwise complain that the lock changed...,
unlock() {
	s3_lock_path=$1
	local_lock_path=$2
  aws s3 rm $s3_lock_path
	$ERR=$?
  rm $local_lock_path
	echo $ERR
}

# lock( s3_lock_path, local_lock_path,id )
# if lock succeds empty string is returned,
# otherwise, string contains error information as follows:
#		if the string has 'errno' in it, the lock failed
#		otherwise the string is just the id of person who locked it.
#	The caller is responsble to handle the already locked state.
lock() {
	s3_lock_pat=$1
	local_lock_pat=$2
	id=$3

	# clear local
  rm $local_lock

  # check remote lock
  #   NB: checking this was has race condition!
  aws s3 cp $s3_lock_path $local_lock
  ERR=$?
  if [ "$ERR" != "0" ] ; then
    # no lock, so create it

		# local
    echo $id > $local_lock # fill lock with your name
		# remote
    aws s3 cp $local_lock $s3_lock_path
    ERR=$?
    if [ "$ERR" != "0" ] ; then
			# cleanup
			rm $local_lock_path
      echo unable to set remote lock: errno $ERR
    fi
		echo "" # indicates no error
  else
    # already locked, return the owner id
    lock_owner=$(cat "$local_lock")
    echo "$lock_owner" # let caller choose to deal with it wait-for or remove lock
  fi
  # assert: video locked
}
