# common code


# Given var $url as an s3_url (s3:// form) to an approved video
if [ "$url" == "" ] ; then
  echo url unassigned
  usage
fi

filename=$(basename "$url")
# alternate filename approach:
#   filename="${url##*/}"
filename_without_extension="${filename%.*}"
extension="${filename##*.}"

kind=$(basename $(dirname "$url"))

filename_len=$(echo -n $filename_without_extension | wc -c)

if [ "$kind" == "" -o "$filename" == "" -o "$filename_without_extension" == "" ] ; then
	echo bad url: $url
	echo kind $kind, filename $filename, filename_without_extension $filename_without_extension, extension $extension
	usage
elif [ $filename_len != 32 ] ; then
	echo bad url: $url
	echo filename_without_extension $filename_without_extension, is $filename_len long, expecting 32 chars
	usage
fi
# export: $input_video_path $video_name $output_frames_dir $kind
