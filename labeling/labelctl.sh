#!/bin/bash
#

function usage()
{
	echo
	echo AI Training Image Pipeline
	echo
	echo Usage:
	echo
	echo " Main flow: "
	echo " $0 [incoming | approve | label | organize_frames | download_frames ]"
	echo "    incoming              - stats on incoming videos"
	echo "    approve [pattern]     - review & approve incoming videos"
	echo "    label   [pattern]     - start labeling the approved videos' frames"
	echo "    organize_frames       - after labeling, organize the frames into the corresponding labels."
	echo "    download_frames       - once the frames organized, download them for training."
	echo
	echo " Note: - You can jump back into the process as needed."
	echo "       - The next step after this is Training. (run labeling/train.sh )"
	echo "       - For info on 'pattern' see below."
	echo
	echo Alternate Usages:
	echo
	echo " Also for incoming videos:"
	echo " $0 [ view | accept_email ]"
	echo "    view [pattern]        - review them."
	echo "    accept_email          - approve them for the given email address."
	echo "                            Does not extract video frames!"
	echo
	echo " Also for approving videos:"
	echo " $0 [ approved | view_approved | pending_extract | extract | extract_frames ]"
	echo "    approved              - stats on approved videos"
	echo "    view_approved   [pat] - review approved videos"
	echo "    pending_extract [pat] - list of all approved videos that need their frames extracted"
	echo "    extract [pattern]     - extract frames from approved videos"
	echo "    extract_frames        - extract frames from the given approved video url (url in s3://... format)"
	echo
	echo " For labeling approved videos:"
	echo " $0 [ pending_label | label_video] | label_stats ]"
	echo "    label_count          - count of frames labeled"
	echo "    pending_label_count [pat]- count of frames to be labeled."
	echo "    pending_label [pat]  - list of frames to be labeled"
	echo "    label_video          - start labeling the extracted frames of the given approved video"
	echo "    label_stats          - more info on the labels being produced"
	echo
	echo "Notes on 'pattern'."
	echo "  - If in square brackets 'e.g. [pat]' it's optional, if not it's required."
	echo "  - To use, add 'pattern xx'".
	echo "	  where any item containing 'xx' will be matched. So, for instance:"
	echo "      $0 view pattern jeffg"
	echo "    will view only those videos 'jeffg'."
	echo
	echo " To be implemented: "
	echo "    lookup                - lookup a video by name and report on it's state"
	echo "    de & relabel          - remove labels from frames and/or relabel the video"
	echo "    skip_video            - skip approval of an incoming video"
	echo "    remove_skipped_video  - remove the skpped video"
	echo
	echo

	exit -1
}

INCOMING_BUCKET="s3://peerwell-incoming-images"
APPROVED_BUCKET="s3://peerwell-approved-videos"
FRAMES_BUCKET="s3://peerwell-ml-frames"
FRAMES_BUCKET_URL="https://s3.amazonaws.com/peerwell-ml-frames" # url version of the same above...
LABELS_BUCKET="s3://peerwell-ml-labels"
LABELED_FRAME_BUCKET="s3://peerwell-ml-frames-labeled"

mkdir -p tmp

#function view_or_approve_or_extract(s3_url,option)
# where option is one of "approve" or "view"
function view_or_approve_or_extract()
{
		s3_url=$1
		option=$2

  	echo copying down $s3_url for $option
  	labeling/extract_frames.sh copy_down $s3_url
		#local_video_path=$(labeling/extract_frames.sh get_local_video_path $s3_url)
		#echo local_video_path $local_video_path 

  	url=$s3_url
  	source labeling/common.sh
  	echo kind $kind
		email=$(basename $(dirname $(dirname "$url") ) )
		echo email $email

		echo viewing video
		echo 
		echo "q)uit, p)ause, p)lay, f)ast, n)ormal, s)low, r)otate, R flip, <spacebar> play, <up> faster, <dn> slower>, - smaller, + bigger, otherwise step"
  	rot_flip_args=$(labeling/extract_frames.sh view $s3_url 1 "${kind}_by_${email}" )
  	echo rotate/flip results: $rot_flip_args

  	while true ; do

			if [ "$option" == "approve" ] ; then 
				read -p 'Choose: r(epeat, A)pprove, D)elete, s)kip, q)uit : ' -n 1 choice
			elif [ "$option" == "extract" ] ; then 
				read -p 'Choose: r(epeat, E)xtract, s)kip, q)uit : ' -n 1 choice
			else
				read -p 'Choose: r(epeat, n)ext, q)uit : ' -n 1 choice
			fi
			echo # newline

  		if [ "$choice" == "q" ] ; then
				echo quit
  			exit
  		elif [ "$choice" == "r" ] ; then
				echo review the video again
				echo 
				echo "q)uit, p)ause, p)lay, f)ast, n)ormal, s)low, r)otate, R flip, <spacebar> play, <up> faster, <dn> slower>, otherwise step"
				rot_flip_args=$(labeling/extract_frames.sh view $s3_url 1 "${kind}_by_${email}" )
				echo rotate/flip results:$rot_flip_args
  			continue
  		elif [ "$choice" == "s" -o "$choice" == "n" ] ; then
				echo skip
  			break
  		elif [ "$choice" == "A" -a "$option" == "approve" ] ; then
  			echo approve
  			# copy to the approved area
				aws s3 mv "${s3_url}" "${APPROVED_BUCKET}/${base_url}"
				ERR=$?
				if [ "$ERR" == "0" ] ; then
					# extract frames from the local copy using the rot_flip_args
					labeling/extract_frames.sh extract $url $rot_flip_args
					ERR=$?
					if [ "$ERR" == "0" ] ; then
						labeling/extract_frames.sh copy_up $s3_url
						ERR=$?
					fi
				fi
				if [ "$ERR" != "0" ] ; then
					echo ERROR stopping
					exit
				fi
  			break
  		elif [ "$choice" == "E" -a "$option" == "extract" ] ; then
  			echo extract
				# extract frames from the local copy using the rot_flip_args
				labeling/extract_frames.sh extract $url $rot_flip_args
				ERR=$?
				if [ "$ERR" == "0" ] ; then
					labeling/extract_frames.sh copy_up $s3_url
					ERR=$?
				fi
				if [ "$ERR" != "0" ] ; then
					echo ERROR stopping
					exit
				fi
  			break
  		elif [ "$choice" == "D" -a "$option" == "approve" ] ; then
  			echo delete
  			# remove entirely, or copy to a reject bucket and delete eventually?
				aws s3 rm "${s3_url}"
				ERR=$?
				if [ "$ERR" != "0" ] ; then
					echo ERROR stopping
					exit
				fi
  			break
			else
				echo invalid choice
				continue
  		fi
  	done # while
}

function get_pattern_args() {
	pattern_default=$1
	pattern_cmd=$2
	pattern_arg=$3

	pattern="$pattern_default"
	if [ "$pattern_cmd" != "" ]; then
		if [ "$pattern_cmd" == "pattern" -a "$pattern_arg" != "" ]; then
			pattern=$pattern_arg
		else
			echo bad pattern argument: $pattern_cmd $pattern_arg
			usage
		fi
	fi
	echo $pattern
}

	
#function aggregate_all_label_csvs( temp_dir, output_path ) {
# copy down all the labels, and combine into a single list
function aggregate_all_label_csvs() {
	temp_dir=$1
	output_path=$2

	# create fresh temporary area
	rm -rf "${temp_dir}"
	mkdir -p "${temp_dir}"

	# copy down all the labels, and combine into a single list
	aws s3 cp "${LABELS_BUCKET}" "${temp_dir}" --recursive > /dev/null
	ERR=$?
	if [ "$ERR" != "0" ] ; then
		echo "aws error: $ERR" > /dev/stderr
		exit
	fi

	csvstack ${temp_dir}/* > "${output_path}"

	# cleanup
	rm -rf "${temp_dir}"
}

# --------------------------------------------------------

#if [ "$#" != "1" ] ; then
#	usage
#elif [ "$1" == "incoming" ] ; then
if [ "$1" == "incoming" ] ; then
	labeling/list_emails.sh incoming

# --------------------------------------------------------
elif [ "$1" == "approved" ] ; then
	labeling/list_emails.sh approved

# --------------------------------------------------------
elif [ "$1" == "approve" -o "$1" == "view" ] ; then
	echo ${1} incoming
	pattern=$(get_pattern_args "@" "$2" "$3")
  incoming=`aws s3 ls "${INCOMING_BUCKET}" --recursive | grep "${pattern}" | awk '{print $4}'`
  for base_url in ${incoming}; do
  	s3_url=${INCOMING_BUCKET}/$base_url

		view_or_approve_or_extract "$s3_url" "$1"

  	echo next incoming...
  done

# --------------------------------------------------------
elif [ "$1" == "view_approved" ] ; then
	echo view approved

	pattern=$(get_pattern_args "@" "$2" "$3")
  list=`aws s3 ls "${APPROVED_BUCKET}" --recursive | grep "${pattern}" | awk '{print $4}'`
  for base_url in ${list}; do
  	s3_url=${APPROVED_BUCKET}/$base_url

		view_or_approve_or_extract "$s3_url" "view"

  	echo next approved...
  done

# --------------------------------------------------------
elif [ "$1" == "accept_email" ] ; then
	echo accept_email
	if [ "$2" != "" ]; then
		email=$2
		echo moving incoming videos from $email to accepted
		aws s3 mv "${INCOMING_BUCKET}/${email}" "${APPROVED_BUCKET}/${email}" --recursive
	else
		echo approve_email requires an email address
		usage
	fi

# --------------------------------------------------------
elif [ "$1" == "pending_extract" ] ; then
	# Do NOT echo anything other than the list because the list is consumed by other proesses.
	#echo approved videos needing frame extraction:

	APPROVED_FULL=tmp/_approved_full
	APPROVED_NAMES=tmp/_approved_names
	EXTRACTED=tmp/_extracted
	OUTPUT=tmp/_pending_extract

	pattern=$(get_pattern_args "--" "$2" "$3")

	# approved...
	approved=`aws s3 ls "${APPROVED_BUCKET}" --recursive | grep '@' | awk '{print $4}'`
	for url in ${approved}; do
		echo $url
	done > $APPROVED_FULL
	for url in ${approved}; do
		source labeling/common.sh
		echo $filename_without_extension
	done | sort | uniq > $APPROVED_NAMES
	# frames...
	extracted=$( aws s3 ls "${FRAMES_BUCKET}" | awk '{print $2}' )
	for dir in ${extracted}; do
		basename $dir
	done | sort | uniq > $EXTRACTED
	# show those approved that are not in extracted...
	todo=$(comm -2 -3 $APPROVED_NAMES $EXTRACTED)
	# by their AWS CLI URL (i.e. via s3:// and not https://s3.amazonaws.com
	for name in $todo ; do
			echo ${APPROVED_BUCKET}/$(grep $name $APPROVED_FULL)
	done > $OUTPUT
	if [ "$pattern" == "--" ] ; then
		cat $OUTPUT
	else
		cat $OUTPUT | grep "$pattern"
	fi

# --------------------------------------------------------
elif [ "$1" == "pending_label" ] ; then
	# Do NOT echo anything other than the list because the list is consumed by other proesses.
	#echo approved videos whose frames need labeling:

	APPROVED_FULL=tmp/_approved_full
	APPROVED_NAMES=tmp/_approved_names
	LABELED=tmp/_labeled
	OUTPUT=tmp/_pending_label

	pattern=$(get_pattern_args "--" "$2" "$3")

	# approved...
	approved=`aws s3 ls "${APPROVED_BUCKET}" --recursive | grep '@' | awk '{print $4}'`
	for url in ${approved}; do
		echo $url
	done > $APPROVED_FULL

	# approved by name
	for url in ${approved}; do
		source labeling/common.sh
		echo $filename_without_extension
	done | sort | uniq > $APPROVED_NAMES

	# labels...
	labels=$( aws s3 ls "${LABELS_BUCKET}" | awk '{print $4}' )
	for url in ${labels}; do
		source labeling/common.sh
		#filename=$(basename $dir)
		#filename_without_extension="${filename%.*}"
		echo $filename_without_extension
	done | sort | uniq > "${LABELED}"

	# show those approved that are not in labels...
	todo=$(comm -2 -3 $APPROVED_NAMES $LABELED)
	# by their AWS CLI URL (i.e. via s3:// and not https://s3.amazonaws.com
	for name in $todo ; do
		echo ${APPROVED_BUCKET}/$(grep $name $APPROVED_FULL)
	done > $OUTPUT
	if [ "$pattern" == "--" ] ; then
		cat $OUTPUT
	else
		cat $OUTPUT | grep "$pattern"
	fi

# --------------------------------------------------------
elif [ "$1" == "pending_label_count" ] ; then

	$0 pending_label $2 $3 | wc -l

# --------------------------------------------------------
elif [ "$1" == "extract_frames" ] ; then
	s3_url=$2
	if [ "$s3_url" == "" ]; then
		echo no s3 url of video provided
		usage
	else
		echo starting the extract process:
		view_or_approve_or_extract "$s3_url" "extract"
	fi

# --------------------------------------------------------
elif [ "$1" == "label_video" ] ; then
	echo label_video
	# eg: labeling/labelctl.sh label_video s3://peerwell-approved-videos/jeffg@inventivity.com/wires/373e8633f918985dea56f32ea572430.MOV
	url=$2
	#id=$3
	if [ "$url" == "" ]; then
    echo "missing arg: approved video url (s3:// fmt)"
		usage
	#elif [ "$id" == "" ]; then
  #  echo "missing arg: id (one of 'jeff,evan,manish')"
	#	usage
	fi

	#
	# From the s3 approved url, figure out where the frames are and make a list
	# of frames to be labeled, convert that list to csv for the labeler, then
	# call the labler. When the labeler is done, upload the results.
	#

	# parse url
	source labeling/common.sh
	video_name=$filename_without_extension
	if [ "$video_name" == "" ] ; then
		echo bad s3 video path: $video_name
		usage
	fi

  ##lock

	
	# ------------------
	# Create the directories and files for holding the input & output csv files.
	#
  LOCAL_LABEL_DIR="tmp/_labels" # stores csvs for labling - input and output
  #KINDOF_INPUT_DIR="${kind}/in" # relative path of the input directory for a kind of video.
  #LOCAL_LABEL_DIR="${LOCAL_LABEL_DIR}/${KINDOF_INPUT_DIR}" # local path to the directories that contain each video's list by kind of video it is.
  LOCAL_LABEL_IN_DIR="${LOCAL_LABEL_DIR}/in" # local path to the directories that contain each video's list by kind of video it is.
  LOCAL_LABEL_OUT_DIR="${LOCAL_LABEL_DIR}/out" # local path to the directories that contain each video's list by kind of video it is.
  LOCAL_LABEL_IN_CSV="${video_name}.csv" # csv with the input list of frames, the video kind it was a member of.
  LOCAL_LABEL_OUT_CSV="${video_name}.csv" # csv with the output list of labeled frames.
  #S3_LABEL_CSV_URL="${LABELS_BUCKET}/${KINDOF_INPUT_DIR}/${video_name}.csv" # url to a directory containing each video's list by kind of video it is.

	# Create local dir for the input labels
	mkdir -p $LOCAL_LABEL_IN_DIR
	mkdir -p $LOCAL_LABEL_OUT_DIR
  csv_in="${LOCAL_LABEL_IN_DIR}/${LOCAL_LABEL_IN_CSV}"
  csv_out="${LOCAL_LABEL_OUT_DIR}/${LOCAL_LABEL_OUT_CSV}"
	# ------------------

	# ------------------
	#
	# Generate the labeling input list from the videos's frames that are actually on S3.
  # The csv columns are:
  #   image_url,kind
  # where:
  #   - image_url is public https url to the frame. THIS MUST BE FIRST for the
  #       labeler to find it
  #   - kind is the kind of video the frame is extracted from
  # row for each frame_url
	#

	# get list of frames
	#list=$(aws s3 ls "${FRAMES_BUCKET_CLI}/$video_name" --recursive | awk '{print $4}')
	list=$(labeling/extract_frames.sh get_list $url)
	if [ "$list" == "" ] ; then
		echo $url has no extracted frames!
		exit
	fi

	# Create (or replace) input csv and add frame to it to label.
	echo image_url,kind > "${csv_in}"
	for i in $list ; do 
		echo "${FRAMES_BUCKET_URL}/$i",$kind
	done >> "${csv_in}"

  # removing prior csv_out
  rm "${csv_out}"

	#
	# -------------------------------------


	#csvs=$(labeling/extract_frames.sh get_list $url)
  #csv_args=($csvs)
  #csv_in=${csv_args[0]}
  #csv_out=${csv_args[1]}
  #echo in  ${csv_in}
  #echo out ${csv_out}


  # see if already done once
  s3_prior="${LABELS_BUCKET}/${video_name}.csv"
  aws s3 cp "$s3_prior" "${csv_out}" 2>errs
  ERR=$?
  if [ "$ERR" == "0" ] ; then
    # already labeled
    echo looking at labels: $s3_prior
    echo already labeled, nothing to do.
		# TODO: provide way to remove prior so that we can re-lablel
  else
    #echo error: $ERR
    #cat errs

    echo no prior labels: starting labeling...
    # label it
	  labeling/label_batch.sh ${csv_in} ${csv_out}
    echo labeling done
    echo uploading labels to: $s3_prior
    aws s3 cp "${csv_out}" "$s3_prior"
    ERR=$?
    if [ "$ERR" != "0" ] ; then
      echo failed to upload labels: $ERR
    fi
  fi
  ##unlock
  exit


  ## If you want to see if it's been labeled by you before or already has a
  ## quorum:
  #LOCAL_LABEL_OUT_DIR="tmp/_labels/prior"
  #mkdir -p "$LOCAL_LABEL_OUT_DIR"
  #source labeling/common.sh
  #video_name=$filename_without_extension
  #local_prior="${LOCAL_LABEL_OUT_DIR}/${video_name}.csv"
  #aws s3 cp "${LABELS_BUCKET}/${video_name}.csv" "${local_prior}"
  #ERR=$?
  #if [ "$ERR" != "0" ] ; then
  #  # no prior
  #  echo "copy prior down error $ERR"
  #  echo "assuming prior doesn't exist"
  #  echo "starting fresh with frames in ${csv_in}"
  #else
  #  # IF WE WANT TO CHECK FOR QUORUMS, this is a start...
  #  # check the prior list and remove anything already labeled by you or has a #
  #  others_prior=$(fgrep -v "$id" "$local_prior")
  #  others_count=$(sort -c -k "$local_prior" )
  #  if [ $other_count > 1 ] ; then
  #    echo $other_count
  #  fi
  #  echo $others_prior
  #  echo $others_count
  #  exit # NOT YET COMPLETE
  #fi
  # quorum in the csv_in (csv_in
  
	#labeling/label_batch.sh ${csv_in} ${csv_out}
  #echo labeling done

  ##merge the output back into downloaded prior
  ##upload the updated prior
  #aws s3 cp "${csv_out}" "${LABELS_BUCKET}/${video_name}.csv"
  ##unlock

# --------------------------------------------------------
elif [ "$1" == "label" ] ; then
	echo label
	#if [ "$2" != "pattern" -o "$3" == "" ] ; then
	#	echo missing arguments
	#	usage
	#fi

	pattern=$(get_pattern_args "--" "$2" "$3")
	if [ "$pattern" == "--" ] ; then
		echo "labeling pending videos"
		list=$($0 pending_label)
	else
		echo "labeling videos matching \"$pattern\""
		list=$($0 pending_label pattern $pattern)
	fi

	for i in $list ; do 
		$0 label_video $i
	done

# --------------------------------------------------------
elif [ "$1" == "extract" ] ; then
	echo extract
	#if [ "$2" != "pattern" -o "$3" == "" ] ; then
	#	echo missing arguments
	#	usage
	#fi

	pattern=$(get_pattern_args "--" "$2" "$3")
	if [ "$pattern" == "--" ] ; then
		echo "extracting frames from pending videos"
		list=$($0 pending_extract)
	else
		echo "extracting frames from pending videos matching \"$pattern\""
		list=$($0 pending_extract pattern $pattern)
	fi

	for i in $list ; do 
		view_or_approve_or_extract "$i" "extract"
	done

# --------------------------------------------------------
elif [ "$1" == "organize_frames" ] ; then
	echo organize_frames

	echo getting list of labeled frames
	LOCAL_LABEL_ALL="tmp/_label_all"
	LOCAL_LABEL_LISTS_DIR="tmp/_label_lists"
	aggregate_all_label_csvs ${LOCAL_LABEL_LISTS_DIR} ${LOCAL_LABEL_ALL}

	echo $(wc -l ${LOCAL_LABEL_ALL}) labeled frames to process

	# aws s3 mb $LABELED_FRAME_BUCKET
	echo "clearing $LABELED_FRAME_BUCKET"
	aws s3 rm $LABELED_FRAME_BUCKET --recursive > /dev/null
	ERR=$?
	if [ "$ERR" != "0" ] ; then
		echo "error: $ERR: clearing $LABELED_FRAME_BUCKET"
		exit
	fi

	# for each row after the csv header line, parse out the image_url,kind,label
	# NOTE:
	# Convert the url from a public url to an s3_url. That is from this:
	#		https://s3.amazonaws.com/BUCKET/FILE_PATH
	# to this:
	#		s3://BUCKET/FILE_PATH
	echo starting to copy and reorganize frames
	count=0
	count_skip=0
	count_file=0
	parallel_count=0
	cat "${LOCAL_LABEL_ALL}" | sed 's/https:\/\/s3.amazonaws.com\//s3:\/\//' | while IFS=, read url kind label;do
    #echo $url $kind $label
		if [ $url == "image_url" ] ; then
			# skip the csv header line.
			count_file=$((count_file+1))
			echo "${count}: file ${count_file}"
			continue
		elif [ $label == "skip" ] ; then
			count_skip=$((count_skip+1))
			echo "${count}: skipping ${count_skip}"
			continue
		fi
		count=$((count+1))
		echo "${count}: $url $kind => $label" 

		#
		# copy to LABELED_FRAME_BUCKET organized by label:
		#		try to run in parallel
		#
		parallel_limit=8
		filename=$(basename $url)
		dest_dir="${LABELED_FRAME_BUCKET}/${label}"
		aws s3 cp "$url" "${dest_dir}/${filename}" > /dev/null &
		pid=$!
		ERR=$?
		if [ "$ERR" != "0" ] ; then
			echo "error: $ERR: copying $url to $dest_dir"
			break
		fi
		PID_LIST+=" $pid";

		parallel_count=$((parallel_count+1))
		if [ $parallel_count -ge $parallel_limit ] ; then
			wait
			parallel_count=0
		else
			echo $parallel_count
		fi

	done
	echo final wait
	wait

# --------------------------------------------------------
elif [ "$1" == "download_frames" ] ; then
	echo download_frames

	# setup local destination for labeled frames
	LOCAL_LABELED_FRAMES="tmp/labeled_frames"
	echo clearing local labeled frames
	rm -rf "${LOCAL_LABELED_FRAMES}"
	mkdir -p  "${LOCAL_LABELED_FRAMES}"

	echo copying down labeled frames
	aws s3 sync "${LABELED_FRAME_BUCKET}" "${LOCAL_LABELED_FRAMES}"
	ERR=$?
	echo err $ERR

	echo "total labeled frames downloaded: $(find "${LOCAL_LABELED_FRAMES}" -type f | wc -l)"

# --------------------------------------------------------
elif [ "$1" == "label_count" ] ; then
	echo label_count

	LOCAL_LABEL_ALL="tmp/_label_all"
	LOCAL_LABEL_LISTS_DIR="tmp/_label_lists"
	aggregate_all_label_csvs ${LOCAL_LABEL_LISTS_DIR} ${LOCAL_LABEL_ALL}

	echo "frames by label:"
	echo "  which labels have the most labeled frames"
	cat "${LOCAL_LABEL_ALL}" | csvcut -c "result" | sort | uniq -c | sort -nr

elif [ "$1" == "label_stats" ] ; then
	echo label_stats

	LOCAL_LABEL_ALL="tmp/_label_all"
	LOCAL_LABEL_LISTS_DIR="tmp/_label_lists"
	aggregate_all_label_csvs ${LOCAL_LABEL_LISTS_DIR} ${LOCAL_LABEL_ALL}


	echo "frames by video kind:"
	echo "  which video kinds produce the most labeled frames"
	cat "${LOCAL_LABEL_ALL}" | csvcut -c "kind" | sort | uniq -c | sort -nr

	echo "frames by label:"
	echo "  which labels have the most labeled frames"
	cat "${LOCAL_LABEL_ALL}" | csvcut -c "result" | sort | uniq -c | sort -nr

	# count videos with labeling done

	# labels by kind. For each kind, count and rank the labels produced.
	echo "labels by kind:"
	cat "${LOCAL_LABEL_ALL}" | csvcut -c "result,kind" | sort | uniq -c
	echo "labels by kind (ranked):"
	cat "${LOCAL_LABEL_ALL}" | csvcut -c "result,kind" | sort | uniq -c | sort -nr

	# labels by kind. For each kind, count and rank the labels produced.
	echo "kind by labels:"
	cat "${LOCAL_LABEL_ALL}" | csvcut -c "kind,result" | sort | uniq -c

# --------------------------------------------------------
elif [ "$#" == "0" -o "$1" == "help" ] ; then
	usage

# --------------------------------------------------------
else
	echo invalid argument: $1
	usage
fi


exit
