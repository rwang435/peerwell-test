To deliver a model you've created to the developers
===

 1. A model is defined by three files:
  - `.pb` - the tensorflow graph and values
  - `.txt` - the labels, in index order
  - `.pbtxt` - the labels, mapped to indices and strings 
 1. The first two you get when training is done. They are found in `tensorflow-for-poets/tf_files`. For instance:
  - retrained_graph-2018-04-12-15-42.pb
  - retrained_labels-2018-04-12-15-42.txt
  - they are named followed by a date in Y-M-D-H-M order
 1. The `.pbtxt` file must created by hand and is named the same as the `.pb` file. For instance: 
  - `retrained_graph-2018-04-12-15-42.pbtxt`
 1. When you have all three, save them in the google drive and share them with developers, and with Jeff as he will organize them with prior models we've built and put into the apps.

Thats it!  Details on how to build the `.pbtxt` file follow:

### .pbtxt File Structure
 
 
 Below is a working sample .pbtxt file.  You can use it as is so long as the labels have not changed, and the ordering of the labels in the `.txt` file is the same.
 
 It is a list of `item` objects in JSON format.
 They should be in same order as that found in the `.txt` file.
 
  - `name` is the is the label name. Taken from the `.txt` file in order.
  - `id` starts at 1 and increments for each subsequent item
  - `display_name` is a structured string the apps use. It has a special format: `label_type - description`.
   -  `label_type` is one of: `ok`, `check`, `bad`.  The latter two are kinds of hazards that the app will display .  `ok` is generally not displayed by the app.
   -  if the item is to be displayed, the `description` is a short string that the app uses to display when the recognition occurs. At least that is true of the python desktop hazard app, and possibly in the iOS & Android apps as well  - check with devs.  The space it's shown in is small so keep it short and succinct.

Below is the working `.pbtxt` file 
 
 ```
 item {
  name: "bad_floor"
  id: 1
  display_name: "bad - floor is cluttered or not smooth"
}
item {
  name: "bad_stair_railing"
  id: 2
  display_name: "bad - stair lacks dual railing"
}
item {
  name: "bad_stair_blocked"
  id: 3
  display_name: "bad - stair is cluttered or blocked"
}
item {
  name: "blur"
  id: 4
  display_name: "ok - blur"
}
item {
  name: "check_carpeted_stair_is_secure"
  id: 5
  display_name: "check - check carpet is well fastened"
}
item {
  name: "check_floor_wires_out_of_way"
  id: 6
  display_name: "check - check floor wires out of way"
}
item {
  name: "check_throw_rug_is_secure"
  id: 7
  display_name: "check - check throw rug is secure"
}
item {
  name: "check_wall_mounted_railing"
  id: 8
  display_name: "check - check wall railing is secure"
}
item {
  name: "good_bath_mat"
  id: 9
  display_name: "ok - bath mat"
}
item {
  name: "good_bathroom"
  id: 10
  display_name: "ok - bathroom"
}
item {
  name: "good_floor"
  id: 11
  display_name: "ok - floor looks clear"
}
item {
  name: "good_grab_bars"
  id: 12
  display_name: "ok - grab bars present"
}
item {
  name: "good_stairs"
  id: 13
  display_name: "ok - stairs"
}
item {
  name: "nil"
  id: 14
  display_name: "ok - nil"
}
```