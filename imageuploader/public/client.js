// Requires jQuery and blueimp's jQuery.fileUpload

// // client-side validation by fileUpload should match the policy
// // restrictions so that the checks fail early
// var acceptFileType = /.*/i;
// var maxFileSize = 1000;

// The URL to your endpoint that maps to s3Credentials function
var credentialsUrl = '/s3_credentials';
//// The URL to your endpoint to register the uploaded file
//var uploadUrl = '/upload';

// input_id is the id of the input element with that
//  selects the file to upload.
window.initS3FileUpload = function( input_id, result_id, credit_str, group_str ) {
  input = $(input_id)
  input.fileupload({
    // acceptFileTypes: acceptFileType,
    // maxFileSize: maxFileSize,
    paramName: 'file',
    add: function( e, data) {
      s3add( e, data, result_id, credit_str, group_str )
    },
    dataType: 'xml',
    done: function(e, data ) {
      onS3Done( e, data, result_id)
    }
  });
};

// Obtain the s3 parameters from our server and append them to the upload form.
function s3add(event, data, result_id, credit_str, group_str ) {
  var filename = data.files[0].name;
  var contentType = data.files[0].type;
  var params = [];
  console.log('s3Add' +" "+ filename +" "+ contentType)
  //console.log('s3Add' +" "+ JSON.stringify(event) )
  //console.log('s3Add' +" "+ JSON.stringify(data) )
  $.ajax({
    url: credentialsUrl,
    type: 'GET',
    dataType: 'json',
    data: {
      filename: filename,
      credit_str:credit_str,
      group_str:group_str,
      content_type: contentType
    },
    error: function(jqXHR, textStatus, errorThrown) {
      console.log("s3Add err: ", JSON.stringify(jqXHR));
      $('<div/>').text(textStatus +": "+ errorThrown ).appendTo( $(result_id) );
    },
    success: function(s3Data) {
      // submit to s3!
      $('<div/>').text('Uploading...').appendTo( $(result_id) );
      //console.log('s3Add ok' +" "+ JSON.stringify(s3Data) )
      data.url = s3Data.endpoint_url;
      data.formData = s3Data.params;
      //console.log('s3Add ok, submitting:'+" "+data.url+" "+JSON.stringify(data.formData) );
      data.submit();
    }
  });
  return params;
};

function onS3Done(event, data, result_id) {
  var s3Url = $(data.jqXHR.responseXML).find('Location').text();
  var s3Key = $(data.jqXHR.responseXML).find('Key').text();
  //console.log( JSON.stringify(event) )
  //console.log( JSON.stringify(data) )
  //console.log( result_id )
  // Typically, after uploading a file to S3, you want to register that file with
  // your backend. Remember that we did not persist anything before the upload.
  //  $('<a/>').attr('href', s3Url).text('File uploaded at '+s3Url+', '+s3Key).appendTo($('body'));
  $('<div/>').text('File uploaded!').appendTo( $(result_id) );
};
