#!/bin/bash
#
# deploy the join app

################
# Get args
ARGS="ok"
if [ $# -ne 2 ] ; then
	ARGS='missing args'
elif [ "$1" != "development" ] && [ "$1" != "staging" ] && [ "$1" != "production" ] ; then
	ARGS="arg 1 bad: $1"
elif [ "$2" != "s3" ] && [ "$2" != "self" ] ; then
	ARGS="arg 2 bad: $2"
fi

if [ "$ARGS" != "ok" ] ; then
	echo bad args $ARGS
	printf "\nusage: $0 deploy_target s3|self\n"
	printf "\tdeploy_target - one of: development | staging | production\n"
	printf "\ts3|self - hosting by s3 or by our self\n"
	printf "\nNB: must be run from the top of repo, aka DEVROOTDIR !\n"
	printf "\nNB: self hosting is on our own ec2 server, while s3 has it's own hosting!\n"
	exit 1
fi
SYSTEM=$1
IS_S3HOSTED=$2

################
# identify targets

if [ "${IS_S3HOSTED}" == "s3" ] ; then
	S3_TARGET="s3://peerwell-uploader"	# location for s3 hosting - configurable!
	IS_S3HOSTED=yes
else
	S3_TARGET="s3://peerwell-uploader"	# staging area for self-hosting - configurable!
	IS_S3HOSTED=no
fi
echo This script is not yet complete!
echo try: 'aws s3 sync . s3://peerwell-uploader'
exit
#DEVTARGET="../peerwell-http-server/build"	# configurable! Will be removed and re-written
#SRC_BUILD="build"	# configurable!
SRC_CODE="public" # configurable
DEPLOYMENT_GROUP_NAME=$S3_TARGET # hack until I start using CodeDeploy

###############
# notify on screen and via alerts about the deployment
MACHINE_NAME=`uname -n`
REPO=`basename -s .git $(git remote show -n origin | grep Fetch | cut -d: -f2-)`
BRANCH=`git branch | grep \* | cut -d ' ' -f2-`
LAST_COMMIT=`git log -1 --pretty=oneline`

DEPLOY_NOTE="Starting deploy to '${DEPLOYMENT_GROUP_NAME}' from '${MACHINE_NAME}' of the repo '${REPO}', branch '${BRANCH}' where the last commit is '${LAST_COMMIT}'"
echo $DEPLOY_NOTE
alert $DEPLOY_NOTE
# Create a file that will label this deployment:
#	Save the deploy version info in a file that will be copied to the server where it can checked.
echo $DEPLOY_NOTE > deploy_version.txt

#
# Standard deploy boilerplate
################

#############
# Configure the code for the right API
CONFIG_DIR=src/config
if [ "$SYSTEM" == "production" ] ; then
	CONFIG_SRC=$CONFIG_DIR/config.production.js
elif [ "$SYSTEM" == "staging" ] ; then
	CONFIG_SRC=$CONFIG_DIR/config.staging.js
else
	CONFIG_SRC=$CONFIG_DIR/config.development.js
fi
CONFIG_TARGET=$CONFIG_DIR/config.current.js
CONFIG_RESET=$CONFIG_DIR/config.development.js # reset back to development
echo "configuring join app for $SYSTEM"
cp "$CONFIG_SRC" "$CONFIG_TARGET"

#############
# Do a build: compile and minify into 'build' directory

# 1. Requires the right version of node and npm 
source  ~/.nvm/nvm.sh
nvm use 'v6.11.0'
ERR=$?
if [ "$ERR" != "0" ] ; then
	ERRSTR="nvm use failed - error code : $ERR"
	echo "${ERRSTR}"
	exit
fi

# 2. Get all the dependencies
npm install
ERR=$?
if [ "$ERR" != "0" ] ; then
	ERRSTR="npm install failed - error code : $ERR"
	echo "${ERRSTR}"
	alert "${ERRSTR}"
	exit
fi

# 3. As part of building, generate files used by app to check it's version and provides notes about the version.
echo 'Prior version is this:'
cat build/version
echo 'Enter the version number of this deploy in semver format -- just the version number (e.g. 3.5.4). Hit Enter to end entry.)'
read VERSION
VERSION=`echo $VERSION | xargs` # trim whitespace
if [ "$VERSION" == "" ] ; then
	echo "Enter valid semver version number, like: 3.5.2"
	exit 1
fi
echo Version will be \"${VERSION}\"
VERSION_STRING='{"deploy_date":"'`date -u`'","version":"'$VERSION'"}'
echo "${VERSION_STRING}" > $SRC_CODE/version.json
#
echo 'Prior version update notes are:'
cat build/updates
echo 'Enter new version update notes below. Tabs and newlines will be displayed. Ctrl-d to end entry.)'
cat > $SRC_CODE/updates
echo
echo ok

# 4. Build the deployable code...
npm run build
ERR=$?
if [ "$ERR" != "0" ] ; then
	ERRSTR="npm build failed - error code : $ERR"
	echo "${ERRSTR}"
	alert "${ERRSTR}"
	exit
fi

# 4. Post build, add some stuff to the deployable directory
# nothing needed...


# Deploy!

########################
# For development:
# run from DEVROOTDIR of peerwell-webjoin
#
if [ "$SYSTEM" == "development" ] ; then
	echo deploying to $DEVTARGET
	rm -rf $DEVTARGET
	cp -Rp ${SRC_BUILD} ${DEVTARGET}
	echo deployed to $DEVTARGET
fi
#

########################
# For production:

if [ "$SYSTEM" == "production" ] || [ "$SYSTEM" == "staging" ]; then
	# copy to target area:
	aws s3 rm ${S3_TARGET} --recursive # clean prior install
	aws s3 cp ${SRC_BUILD} ${S3_TARGET} --recursive
	ERR=$?
	if [ "$ERR" == "0" ] ; then
		if [ "$IS_S3HOSTED" != 's3' ] ; then
			# We are self hosting, so
			# run join_deploy_pull.sh to pull the code in from s3 and install it properly
			STR="Ready - SSH into the $SYSTEM system and run join_deploy_pull.sh in the apputils/imageserver directory to complete install."
		else
			STR="Success!"
		fi
		echo "${STR}"
		alert "${STR}"
	else
		STR="Failed - copy to s3 failed - error $ERR"
		echo "${STR}"
		alert "${STR}"
	fi

	# Restore development config for local operation
	CONFIG_TARGET=$CONFIG_DIR/config.current.js
	echo "resetting join configuration from $SYSTEM to development"
	cp "$CONFIG_RESET" "$CONFIG_TARGET"

fi
