#!/bin/bash
#

# Pull from s3

for i in package.json imageserver.js s3.js server.js; do
	echo $i
	aws s3 cp s3://peerwell-uploader/$i $i
done

aws s3 cp s3://peerwell-uploader/public public --recursive

