var express = require('express');
var crypto = require('crypto');
var path = require('path');

var s3 = require('./s3');

// env PORT=4731 S3_ACCESS_KEY="AKIAJNBVPRDFEOCR3D2Q" S3_SECRET_KEY="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" S3_BUCKET="peerwell-incoming-images" S3_REGION="us-east-1" node imageserver.js
var s3Config = {
  accessKey: process.env.S3_ACCESS_KEY,
  secretKey: process.env.S3_SECRET_KEY,
  bucket: process.env.S3_BUCKET,
  region: process.env.S3_REGION
};

var app = express();

app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));

function field_ok( f )
{
  //console.log("field_ok: ", f)
  if( typeof(f) != "undefined" && f.length > 0) {
    //console.log("good")
    return true
  }
  //console.log("bad")
  return false
}

app.get('/s3_credentials', function(request, response) {
  // console.log( JSON.stringify(request.query) )
  if( field_ok(request.query.filename) && field_ok(request.query.credit_str) && field_ok(request.query.group_str) )
  //if( field_ok(request.query.filename) )
  {
    var filename =
      request.query.credit_str + '/'
      + request.query.group_str + '/'
      + crypto.randomBytes(16).toString('hex')
      + path.extname(request.query.filename);
    //console.log(filename)
    response.json( s3.s3Credentials( s3Config, {filename: filename, contentType: request.query.content_type}) );
  } else {
    response.status(400).send('filename, credit_str, and group_str are all required');
  }
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});
