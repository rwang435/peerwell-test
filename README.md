# PeerWell ML Repo

Intended as the repo for all components of Project Hazel.

- **imageuploader**: This is code to enable outsiders to store images and videos so that they can be used for training.
- **imagebin**: image utilities related to recognition needs.
  - python tool for downloading images from google
  - copying found images to/from S3
  - python tool for extracting images (frames) from videos
- **tensorflow-for-poets**: place where we make ML models trained from images
- **labelctl**: controls the entire labeling process, starting from when we:
  1. receive an incoming video submission
  2. approving that submission
  3. extracting frames for labeling
  4. finding batches of images to label
  5. doing the labeling
  6. selecting images to train from
  7. copying images to the training area to start model building

# Installation

see: ./install.txt

