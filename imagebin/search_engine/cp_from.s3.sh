#!/bin/bash
#
# copy from s3 to the local tree of images to s3
#

S3_BUCKET=s3://peerwell-hazard-ml/images

if [ "$1" == "" ] ; then
	echo usage: $0 from
	echo '     ' from - the s3 bucket url
	echo
	echo provide the url of the s3 bucket
	echo
	echo ls $S3_BUCKET
	aws s3 ls ${S3_BUCKET} --recursive --human-readable
	echo
	exit
fi

# copy from the bucket to tmpe file, then unzip
aws s3 cp $1 tmp.zip
unzip tmp.zip

