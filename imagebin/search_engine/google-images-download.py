
# coding: utf-8

# In[ ]:

#Searching and Downloading Google Images/Image Links

#Import Libraries

#coding: UTF-8

import time       #Importing the time library to check the time of code execution
import sys    #Importing the System Library
import os
import urllib2


########### Edit From Here ###########

#This list is used to search keywords. You can edit this list to search for google images of your choice. You can simply add and remove elements of the list.
search_keyword = [
    # CONSIDER HANDHOLDS and GRIPS to find safe paths!! # jg 1/13/2018
    # Q: How To collect images for learning ? Just a closeup, angles, with
    # other items? How Pure? What is Pure?
    # reflections are useful...
    # slight roatations of the floor shots would help


    ## --- good_bathroom --- 
    #'bathroom "with grab bars"',
    #'bathtub "with mat"',
    #'shower "with mat"',
    #'in tub bath mat',
    #'grab bars',

    ## --- bad_bathroom_without_tub_mat --- 
    #'bathtub "without mat"',
    #'shower "without mat"',
    #'bathtub',
    #'shower',
    #'bathtub floor', # Jan 5
    #'shower floor', # Jan 5

    ## --- bad_bathroom_without_grab_bars --- 
    #'bathroom "without grab bars"',
    #'bathroom',
    #'bathroom wall', # Jan 5

    ## --- good_stairway --- 
    #'home stairway "with railing"',
    #'stair treads carpet', # Jan 15, 2018
    #'stair treads wood', # Jan 15, 2018
    #'stair railing wall mounted', # Jan 15, 2018
    #'stair railing on wall', # Jan 15, 2018
    #'stairs looking up', # Jan 15, 2018
    #'stairs looking down', # Jan 15, 2018
    #'stairs down railing', # Jan 19, 2018
    #'stairs up railing', # Jan 19, 2018
    #'clean stairs', # Jan 19, 2018
    #'dirty stairs', # Jan 19, 2018

    ## --- bad_stair_with_clutter --- 
    #'home stairway "with clutter"',
    #'hoarder stairs',
    #'cluttered stairs',
    #'untidy stairs',
    #'obstructed staircase',
    #'blocked staircase',
    #'staircase tripping hazard',
    #'stairs cluttered', # Jan 15, 2018
    #'stairs blockage', # Jan 15, 2018
    #'stairs gate', # Jan 15, 2018
    ## --- bad_stair_without_railing --- 
    #'home stairway "without railing"',
    #'stairs without railing', # Jan 15, 2018

    ## --- check_stair_runner_is_secured ---  # NOT DONE
    #'stair runners', # NOT DONE

    ## --- badly lit room ---
    #'poorly lit room',
    #'dark lit room',
    #'dim lighting',
    #'candle lighting',
    #'too dark room', # Jan 15, 2018
    #'badly lit hallway', # Jan 15, 2018
    #'darkness living room', # Jan 15, 2018
    #'darkness office', # Jan 15, 2018
    #'darkness bathroom', # Jan 15, 2018
    #'darkness kitchen', # Jan 15, 2018
    #'darkness stair', # Jan 16, 2018

    ## --- good_floor --- # Clear & uncluttered
    #'"tiled OR wooden" floor', # Dec 28, 2017
    #'proper wooden floor', #vs floor surface shots in bad_floor_damage #jan 11
    #'hardwood floor', #vs floor surface shots in bad_floor_damage #jan 11
    #'tiled floor', #vs floor surface shots in bad_floor_damage #jan 11
    #'carpet', #vs floor surface shots in bad_floor_damage #vs throw rug, vs damaged rug
    #'wall to wall carpet', # Jan 15, 2018
    'floor railing',
    #
    # These are too complicated... i'm removing them for now...
    #'carpeted room',  # vs room pics of bad_floor_with_clutter #jan 11
    #'uncluttered floor', # vs room pics of bad_floor_with_clutter #jan 11
    #'uncluttered interior', # vs room pics of bad_floor_with_clutter #jan 11
    #'uncluttered room',
    ## --- bad_floor_with_clutter --- 
    #'cluttered floor',
    #'clothes on floor',
    #'strewn on floor',
    #'hoarder floor',
    ## --- bad_floor_with_damage --- 
    #'damaged floor', # jan 11
    #'wood floor damage', #jan 11
    #'tile floor damage', #jan 11
    #'lineoleum floor damage' # not so good
    #'rug with holes',
    #'frayed rug',
    ## --- check_floor_wires_out_of_way --- 
    #'floor loose wires',
    #'wires OR "extension cords" OR "power cords" "on floor"'
    #'floor wires untidy'
    #'power strip mess', #jan 10
    #'extension cord mess', #jan 10
    #'power strip with cords', #jan 10
    ## --- check_throw_rug_is_secure --- 
    #'throw rug',
    #'hallway runners', #jan 11

    ## --- unused --- 
    ##'low bed', # not knee height
    ]


# This list is used to further add suffix to your search term. Each element of
# the list will help you download 100 images. First element is blank which
# denotes that no suffix is added to the search keyword of the above list. You
# can edit the list by adding/deleting elements from it.So if the first element
# of the search_keyword is 'Australia' and the second element of keyword_suffixes is 'high resolution', then it will search for 'Australia High Resolution'
#keyword_suffixes = [' high resolution']
keyword_suffixes = ['']

########### End of Editing ###########




# Downloading entire Web Document (Raw Page Content)
def download_page(url):
    version = (3,0)
    cur_version = sys.version_info
    if cur_version >= version:     #If the Current Version of Python is 3.0 or above
        import urllib.request    #urllib library for Extracting web pages
        try:
            headers = {}
            headers['User-Agent'] = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"
            req = urllib.request.Request(url, headers = headers)
            resp = urllib.request.urlopen(req)
            respData = str(resp.read())
            return respData
        except Exception as e:
            print(str(e))
    else:                        #If the Current Version of Python is 2.x
        import urllib2
        try:
            headers = {}
            headers['User-Agent'] = "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 Safari/537.17"
            req = urllib2.Request(url, headers = headers)
            response = urllib2.urlopen(req)
            page = response.read()
            return page
        except:
            return"Page Not found"


#Finding 'Next Image' from the given raw page
def _images_get_next_item(s):
    start_line = s.find('rg_di')
    if start_line == -1:    #If no links are found then give an error!
        end_quote = 0
        link = "no_links"
        return link, end_quote
    else:
        start_line = s.find('"class="rg_meta"')
        start_content = s.find('"ou"',start_line+1)
        end_content = s.find(',"ow"',start_content+1)
        content_raw = str(s[start_content+6:end_content-1])
        return content_raw, end_content


#Getting all links with the help of '_images_get_next_image'
def _images_get_all_items(page):
    items = []
    while True:
        item, end_content = _images_get_next_item(page)
        if item == "no_links":
            break
        else:
            items.append(item)      #Append all the links in the list named 'Links'
            time.sleep(0.1)        #Timer could be used to slow down the request for image downloads
            page = page[end_content:]
    return items


############## Main Program ############
t0 = time.time()   #start the timer

#Download Image Links for each keyword phrase and suffix pair
for i in xrange( 0, len( search_keyword ) ):

    items = []
    iteration = "Item no.: " + str(i+1) + " -->" + " Item name = " + str(search_keyword[i])
    print (iteration)
    print ("Evaluating...")
    search_keywords = search_keyword[i]
    search = search_keywords.replace(' ','%20')
    
    #make a search keyword results directory
    try:
        os.makedirs(search_keywords)
    except OSError, e:
        if e.errno != 17:
            raise   
        # time.sleep might help here
        pass
    
    # Add the suffix, if any, and search...
    for j in xrange( 0, len( keyword_suffixes ) ):
        search_suffix = keyword_suffixes[j].replace(' ','%20')

        # google search url
        #   q=search_string
        #   espv = ?
        #   biw, bih # width and height of something
        #   site=webhp
        #   source=lnms
        #   tbm=isch #Images:
        #   sa=X
        #   ei=XosDVaCXD8TasATItgE
        #   ved=0CAcQ_AUoAg
        url = 'https://www.google.com/search?q=' + search + search_suffix + '&espv=2&biw=1366&bih=667&site=webhp&source=lnms&tbm=isch&sa=X&ei=XosDVaCXD8TasATItgE&ved=0CAcQ_AUoAg'
        print (url)

        raw_html =  (download_page(url))
        time.sleep(0.1)

        items = items + (_images_get_all_items(raw_html))

    #print ("Image Links = "+str(items))
    print ("Total Image Links = "+str(len(items)))
    print ("\n")


    # Dump the links into a file within the search directory.
    url_list = search_keywords
    url_list += "/urls.txt"
    info = open(url_list, 'a')        #Open the text file called database.txt
    info.write(str(i) + ': ' + str(search_keyword[i-1]) + ": " + str(items) + "\n\n\n")         #Write the title of the page
    info.close()                            #Close the file

    t1 = time.time()    #stop the timer
    total_time = t1-t0   #Calculating the total time required to crawl, find and download all the links of 60,000 images
    print("Total time taken: "+str(total_time)+" Seconds")
    print ("Starting Download...")

    ## To save imges to the same directory
    # IN this saving process we are just skipping the URL if there is any error

    errorCount=0
    k=0
    while( k < len(items) ):
        from urllib2 import Request,urlopen
        from urllib2 import URLError, HTTPError

        try:
            print( items[k] )
            req = Request(items[k], headers={"User-Agent": "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 Safari/537.17"})
            response = urlopen(req,None,15)
            output_file = open(search_keywords+"/"+str(k+1)+".jpg",'wb')
            
            data = response.read()
            output_file.write(data)
            response.close();

            print("completed ====> "+str(k+1))

            k=k+1;

        except IOError:   #If there is any IOError

            errorCount+=1
            print("IOError on image "+str(k+1))
            k=k+1;

        except HTTPError as e:  #If there is any HTTPError

            errorCount+=1
            print("HTTPError"+str(k))
            k=k+1;
        except URLError as e:

            errorCount+=1
            print("URLError "+str(k))
            k=k+1;


print("\n")
print("Everything downloaded!")
print("\n"+str(errorCount)+" ----> total Errors")

#----End of the main program ----#


# In[ ]:



