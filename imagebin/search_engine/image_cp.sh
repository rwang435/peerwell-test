#!/bin/bash
#
# Copy and rename training images from a source directory organization to the learning directory organization.
#
#	Tensorflow for Poets training organization has one directory per category, with all images in each
#	The Source directories have one directory per category and then
#		under each a directory representing the google search used to obtain the images. 
#		Within each subdirectory, images are in jpg format, labeled from 1-n.jpg, and may be discontinuous.
#		In addition this tool (google-images-download.py) creates a list, mapping each image to a url
#
#	We want to preserve the mapping, so, you need to move all the search directories under an appropirate  category directory.
#		Then if you run ths script it will create under each of these a 'bad' directory. You can copy any image in the search
#		directoy into the it's 'bad' directory. Any image in the bad directories will not be copied to the training area.
#
#		Everytime you run this script it will copy category_directory/search_directories/*.jpg into the target directory's
#			category directory with the name "n.jpg", where 'n' is increasing by 1 each time.
#
# NB: While you can remove items to the 'bad' directories, editing an item is another matter, since it's not reproducible.
#		Might create an 'orig' subdir within each subdir to preserve any edited ones. And if editing, add something to
#		the list to note that it is modified.
#	
# So we get reproducibility by copying to the training area. The #s may change in the training area but the content and tracking
#		is preserved in the source area.



#########
# arguments
cd images
CATEGORY_DIRS=`echo bad*/ check*/ good*/`  # MUST END IN '/' OR THIS SCRIPT WILL FAIL!

# where to copy to
TRAIN_DIR=../../tensorflow-for-poets-2/tf_files/hazards/
# arguments: end
#########

function usage () {
	echo
	echo "usage: $0 --[copy|setup]"
	echo " One argument required:"
	echo " --setup : Adds a \"misc\" directory to each classification to hold good images found through manual serach."
	echo "         : Adds a \"bad\" directory to each search directory."
	echo "         : Displays count of good images in each classification."
	echo "         : This command is non-destructive"
	echo " --count : Displays count of good images in each classification and search."
	echo " --copy  : copies all classification's images,"
	echo "            except for those in each search's \"bad\" directory,"
	echo "            to the corresponding training directory."
	echo "         : This command is destructive! Removes previous training image directory."
	echo
	echo "Classifications are:"
	for cat in $CATEGORY_DIRS ; do
		echo "  `basename $cat`"
	done
	echo
	echo "Training dir is: $TRAIN_DIR"

  exit
}

if [ $# -ne 1 ] ; then
	echo wrong argument count $#
	usage
else
	if [ "$1" == "--setup" ] ; then
		action="setup"
	elif [ "$1" == "--copy" ] ; then
		action="copy"
	elif [ "$1" == "--count" ] ; then
		action="count"
	else
		echo bad argument
		usage
	fi
fi

# ----------------------

if [ "$action" == "copy" ] ; then
	echo Recreating training directory: "$TRAIN_DIR"
	rm -r "$TRAIN_DIR"
	mkdir -p "$TRAIN_DIR"
else
	echo Setting up directories for each classification and search, and counting images in each classification...
fi

for cat_dir in ${CATEGORY_DIRS} ; do
	#echo $cat_dir/*/

	# Setup 1 of 2 post-search directories for image management...
	mkdir -p "$cat_dir"misc # for good images found manually, not via automated search ...

	# setup training category directories...
	train_path="$TRAIN_DIR""$cat_dir"
	#echo $train_path
	mkdir -p "$train_path"

	# For each automated search term image result and for the manual misc directory of images ....
	cat_count=0
	SEARCH_DIRS=`echo "${cat_dir}*/"` # must end in slash, '/'
	echo  "$cat_dir",,, # category, search term, category total, search total
	for search_dir in ${SEARCH_DIRS} ; do
		#echo "${search_dir}"
		cat_search_count=0

		# Setup 2 of 2 post-search directories for image management...
		#		Create special directory for bad images found in search that don't belong: we're going to skip those in there.
		#		As part of a manual process you put them in here so they're accounted for, but excluded from the training.
		mkdir -p  "$search_dir"bad

		# Copy all those
		for file in ./"$search_dir"*.jpg; do
			# if file exists, copy it to proper place & name

			# skip if none found:
			[ -e "$file" ] || continue

			let "cat_count++"
			let "cat_search_count++"

			# copy...
			if [ "$action" == "copy" ] ; then
				target="${train_path}${cat_count}.jpg"
				#echo cp "$file" "$target"
				cp -- "$file" "$target"
			fi

		done
		if [ "$action" == "count" ] ; then
			echo  ,"${search_dir}",,${cat_search_count} # category, search term, category total, search total
		fi

	done
	echo  "$cat_dir",,${cat_count} # category, search term, category total, search total

	# remove anything category that is too small to succeed
	if [ "$action" == "copy" ] ; then
		if [ ${cat_count} -lt 20 ] ; then
			echo "Image count < 20, removing: $train_path"
			rm -r "$train_path"
		fi
	fi

done
