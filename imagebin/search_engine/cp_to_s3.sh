#/bin/bash
#
# copy local tree of images to s3
#


# zip the directory
DIR=images
#DATED=`date "+%Y-%m-%d-%H-%M-%S"`
DATED=`date "+%Y-%m-%d-%H-%M"`
ARCHIVE_FILE=${DIR}-${DATED}.zip
zip -r ${DIR} ${DIR}
mv $DIR.zip ${ARCHIVE_FILE}

# copy to s3
#		aws s3 mb s3://peerwell-hazard-ml
#		aws s3 mb s3://peerwell-hazard-ml/images
S3_BUCKET=s3://peerwell-hazard-ml/images
aws s3 cp $ARCHIVE_FILE ${S3_BUCKET}/${ARCHIVE_FILE}
aws s3 ls ${S3_BUCKET} --recursive --human-readable

