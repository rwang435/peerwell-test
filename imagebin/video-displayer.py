# From: https://github.com/maikbasso/python-video-slicer
# -*- coding: utf-8 -*-

import cv2
import sys
import os
import numpy as np
import signal


# args: video_path, video_name, output_directory
#   video_path - local path to a video from which frames will be extracted.
#   video_name - name of video to be used when generating frames (i.e. basename
#	   wihout the extension.
#   output_dir - path to a directory where each frame will be stored.
#   each frame output is named:
#	   video_name.NNNN.jpg
#   where NNNN is a zero padded frame number

# -------------------------------
# get input args...

def usage():
	print "\nView a video."
	print "	 Given a video, view every nth_frame of it.\n"
	print "usage:"
	print " python video-displayer.py video_path [nth_frame [title]]\n"
	print "	 video_path - local path to a video from which frames will be extracted."
	print "	 nth_frame  - if 1, then every frame; if 2, then every other, etc"
	print "	 title		- title of the video window"
	print "video control keys:"
	print "	 pace:"
	print "	   q - quit"
	print "	   p - pause or resume playing"
	print "	   f - fast speed"
	print "	   n - normal"
	print "	   s - slow"
	print "	   faster - <up-arrow>"
	print "	   slower - <down-arrow>"
	print "	   resume - <spacebar>"
	print "	   step   - <any other key>"
	print "	 rotation - r"
	print "	   hit repeatedly to cycle through rotate right, left, none"
	print "	 flipping - R"
	print "	   hit repeatedly to cycle through flipping horizontal, vertical, both, none"
	print "	 scale:"
	print "	   +      - bigger"
	print "	   =      - bigger"
	print "	   -      - smaller"
	print "	   _      - smaller"
	print "	   hit repeatedly to cycle through flipping horizontal, vertical, both, none"
	exit(-1)

argc = len( sys.argv )
if argc < 2:
	usage()
else:
	video_path = sys.argv[1]
	title = ""
	nth_frame = 1

	if argc > 2:
		nth_frame  = int(sys.argv[2])
	if argc > 3:
		title  = sys.argv[3]

	if title == "":
		title = "INPUT"
	if nth_frame <= 0:
		usage()

# -------------------------------

# Rotate the image and re-adjust the dimensions
def rotate_bound(image, angle):
	# grab the dimensions of the image and then determine the
	# center
	(h, w) = image.shape[:2]
	(cX, cY) = (w // 2, h // 2)
 
	# grab the rotation matrix (applying the negative of the
	# angle to rotate clockwise), then grab the sine and cosine
	# (i.e., the rotation components of the matrix)
	M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
	cos = np.abs(M[0, 0])
	sin = np.abs(M[0, 1])
 
	# compute the new bounding dimensions of the image
	nW = int((h * sin) + (w * cos))
	nH = int((h * cos) + (w * sin))
 
	# adjust the rotation matrix to take into account translation
	M[0, 2] += (nW / 2) - cX
	M[1, 2] += (nH / 2) - cY
 
	# perform the actual rotation and return the image
	return cv2.warpAffine(image, M, (nW, nH))

def rot_flip( frame, which ):
	if which == "horz":
		frame = cv2.flip( frame, 1 )
	elif which == "vert":
		frame = cv2.flip( frame, 0 )
	elif which == "both":
		frame = cv2.flip( frame, -1 )
	elif which == "right":
		frame = rotate_bound( frame, 90 )
	elif which == "left":
		frame = rotate_bound( frame, 270 )
	return frame

# Given an image, scale it by the given width or height.  Cannot give both
# width or height, or it's not scaling, of course!
def image_resize(image, width = None, height = None, inter = cv2.INTER_AREA):
	dim = None # target dimensions we're after
	(h, w) = image.shape[:2] # image dimensions

	# if both the width and height are None, then return the
	# original image
	if width is None and height is None:
		return image

	if width is None:
		# adjust width by the ratio of the given height.
		r = height / float(h)
		dim = (int(w * r), height)

	else:
		# adjust height by the ratio of the given wdith.
		r = width / float(w)
		dim = (width, int(h * r))

	# resize the image
	resized = cv2.resize(image, dim, interpolation = inter)

	# return the resized image
	return resized

## pct can be a float e.g. 1.5 or .5
#def scale( frame, pct ):
#	if pct == 1.0:
#		return frame
#	return cv2.resize(frame, (0,0), pct, pct)

def scale_to( frame, w = None, h = None ):
	if w == None and h == None:
		return frame
	return image_resize( frame, w, h )

def n_to_rot( n ):
	switcher = {
		0: "none",
		1: "right",
		2: "left",
	}
	return switcher.get( n%3, "none" )

def n_to_flip( n ):
	switcher = {
		0: "none",
		1: "vert",
		2: "horz",
		3: "both",
	}
	return switcher.get( n%4, "none" )
# -------------------------------

def give_focus_to_next_window_hack():
	# hack to switch to the cv window...
	cv2.namedWindow("GetFocus", cv2.WINDOW_NORMAL);
	#img = cv2.Mat.zeros(100, 100, cv2.8UC3);
	#cv2.imshow("GetFocus", img);
	cv2.imshow("GetFocus", 0);
	cv2.setWindowProperty("GetFocus", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN);
	cv2.waitKey(1);
	cv2.setWindowProperty("GetFocus", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_NORMAL);
	cv2.destroyWindow("GetFocus");
	# next window will get the focus!

# -------------------------------
framerate_normal=7
framerate_fast=1
framerate_slow=500
framerate_step=50
def framerate( dir, n ):
	if dir == "pause":
		n = 0
	elif dir == "normal":
		n = framerate_normal
	elif dir == "faster" and n > framerate_step:
		n = n - framerate_step
	elif dir == "slower":
		n = n + framerate_step
	elif dir == "fast" and n > framerate_step:
		n = framerate_fast
	elif dir == "slow":
		n = framerate_slow

	# debuggery
	#print "framerate:",dir, n
	return n

# -------------------------------

def handle_interrupt(signal, frame):
	#print('You pressed Ctrl+C!')
	print "Interrupt - no frames processed"
	if cap:
		cap.release()
	cv2.destroyAllWindows()
	sys.exit(-1)


def main():

	global cap; # so it can be handled on from an interrupt handler.

	# read video file
	cap = cv2.VideoCapture(video_path) # FYI: pass int 0 for webcam!
	signal.signal(signal.SIGINT, handle_interrupt)

	give_focus_to_next_window_hack()

	#
	delay_ms = framerate( "pause", 0) # start paused.
	saved_delay_ms = framerate( "normal", 0) # resume at normal speed initially.
	# rotate or flip frame
	rot_flip_1 = rot_flip_2 = "none"
	rot_flip_count_1 = rot_flip_count_2 = 0
	#scale_pct=1.0
	scale_amt=500
	scale_delta=200
	# count frame
	count = 0
	processed = 0
	while(True):
		# Capture frame-by-frame
		ret, frame = cap.read()

		# if frame exists
		if ret == True:

			if nth_frame == 1 or count % nth_frame == 0:

				#sys.stdout.write("count: %d %s %s	 \r" % (count+1, rot_flip_1, rot_flip_2) )
				#sys.stdout.flush()

				if rot_flip_1 != "none":
					frame = rot_flip( frame, rot_flip_1 )
				if rot_flip_2 != "none":
					frame = rot_flip( frame, rot_flip_2 )
				#if scale_pct != 1.0:
				#	frame = scale( frame, scale_pct )
				frame = scale_to( frame, None, scale_amt )

				# Display results
				cv2.imshow( title, frame)
				cv2.moveWindow( title, 0,0)


				processed = processed + 1

			# increment counter
			count = count + 1
		else:
			#print "complete"
			break

		# Give time for the frame to be displayed, otherwise it won't show.
		#   Grab the key, if one received, and use it to control the process.

		#print "delay_ms", delay_ms
		key_ord = cv2.waitKey( delay_ms ) & 0xff
		if key_ord == ord('q'): # quit
			#print "quit"
			break
		elif key_ord == 255: # nothing hit
			continue
		elif key_ord == ord('-') or key_ord == ord('_'): # scale down
			scale_amt = scale_amt - scale_delta
			if scale_amt < scale_delta:
				scale_amt = scale_delta
		elif key_ord == ord('+') or key_ord == ord('='): # scale up
			scale_amt = scale_amt + scale_delta
		elif key_ord == ord('r'): # rot or flip frame
			rot_flip_count_1 = rot_flip_count_1 + 1
			rot_flip_1 = n_to_rot( rot_flip_count_1 )
		elif key_ord == ord('R') or key_ord == ord('F'): # rot or flip frame
			rot_flip_count_2 = rot_flip_count_2 + 1
			rot_flip_2 = n_to_flip( rot_flip_count_2 )
		elif key_ord == ord('p') or key_ord == ord(' '): # pause or play
			if delay_ms <= 0:
				delay_ms = saved_delay_ms
			else:
				saved_delay_ms = delay_ms #save 
				delay_ms = framerate("pause", delay_ms)
		#elif key_ord == ord('T'): # tinier
		#	scale_pct = scale_pct - 0.1
		#	if scale_pct <= 0:
		#		scale_pct = 0.1
		#	print "scale", scale
		#elif key_ord == ord('B'): # bigger
		#	scale_pct = scale_pct + 0.1
		#	print "scale", scale
		#elif key_ord == ord('1'): # 100%
		#	scale_pct = 1.0
		#	print "scale", scale
		else:
			if key_ord == ord('f'): # speed up
				delay_ms = framerate("fast", saved_delay_ms)
			elif key_ord == ord('s'): # slow down
				delay_ms = framerate("slow", saved_delay_ms)
			elif key_ord == ord('n'): # normal
				delay_ms = framerate("normal", saved_delay_ms)
			elif key_ord == 0: # up-arrow
				delay_ms = framerate("faster", saved_delay_ms)
			elif key_ord == 1: # down-arrow
				delay_ms = framerate("slower", saved_delay_ms)

			saved_delay_ms = delay_ms #save 

			# diagnostic / development: show key
			#else:
			#	print ("key", chr(key_ord))

	# cleanup
	cap.release()
	cv2.destroyAllWindows()

	#print "\n" # needed if using sys.stdout.write above
	#print count, "frames, ", processed, ", processed"
	# This output is required as is by the script calling this program for peerwell
	# labeing.
	if count > 0:
		print n_to_rot( rot_flip_count_1), n_to_flip( rot_flip_count_2)
		exit(0)
	else:
		print "no frames processed"
		exit(1)


if __name__ == "__main__":
	main()

