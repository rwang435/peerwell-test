# From: https://github.com/maikbasso/python-video-slicer
# -*- coding: utf-8 -*-

import cv2
import sys
import os
import numpy as np

# args: video_path, video_name, output_directory
#   video_path - local path to a video from which frames will be extracted.
#   video_name - name of video to be used when generating frames (i.e. basename
#	   wihout the extension.
#   output_dir - path to a directory where each frame will be stored.
#   each frame output is named:
#	   video_name.NNNN.jpg
#   where NNNN is a zero padded frame number

# -------------------------------
# get input args...

def usage():
	print "\nExtract frames from a video."
	print "	 Given a video, extract all it's frames into an output directory\n"
	print "usage:"
	print " python video-slicer.py video_path video_name output_dir nth_frame [rot_flip_1 [rot_flip_2]]\n"
	print "	 video_path - local path to a video from which frames will be extracted."
	print "	 video_name - name of video to be used when generating frames (i.e.  basename wihout the extension."
	print "	 output_dir - path to a directory where each frame will be stored."
	print "	 nth_frame  - if 1, then every frame; if 2, then every other, etc"
	print "	 rot_flip   - one of 'vert, horz, both, right, left, none'"
	print "		vert, horz, both: flip the frame\n"
	print "		right, left: rotate the frame\n"
	print "	 Each frame output is named:"
	print "		  video_name.NNNNN.jpg"
	print "	 where NNNNN is a zero padded frame number\n"
	quit()

argc = len( sys.argv )
if argc < 4 or argc > 7:
	usage()
else:
	video_path = sys.argv[1]
	video_name = sys.argv[2]
	output_dir = sys.argv[3]
	nth_frame  = int(sys.argv[4])

	if nth_frame <= 0:
		usage()

	rot_flip_1 = rot_flip_2 = "none"
	if argc == 6:
		rot_flip_1 = sys.argv[5]
	elif argc == 7:
		rot_flip_1 = sys.argv[5]
		rot_flip_2 = sys.argv[6]

# -------------------------------

def mkdir_if_not_present(dir):
	if not os.path.exists( dir ):
		os.makedirs( dir )


# Rotate the image and re-adjust the dimensions
def rotate_bound(image, angle):
	# grab the dimensions of the image and then determine the
	# center
	(h, w) = image.shape[:2]
	(cX, cY) = (w // 2, h // 2)
 
	# grab the rotation matrix (applying the negative of the
	# angle to rotate clockwise), then grab the sine and cosine
	# (i.e., the rotation components of the matrix)
	M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
	cos = np.abs(M[0, 0])
	sin = np.abs(M[0, 1])
 
	# compute the new bounding dimensions of the image
	nW = int((h * sin) + (w * cos))
	nH = int((h * cos) + (w * sin))
 
	# adjust the rotation matrix to take into account translation
	M[0, 2] += (nW / 2) - cX
	M[1, 2] += (nH / 2) - cY
 
	# perform the actual rotation and return the image
	return cv2.warpAffine(image, M, (nW, nH))

def rot_flip( frame, which ):
	if which == "horz":
		frame = cv2.flip( frame, 0 )
	elif which == "vert":
		frame = cv2.flip( frame, 1 )
	elif which == "both":
		frame = cv2.flip( frame, -1 )
	elif which == "right":
		frame = rotate_bound( frame, 90 )
	elif which == "left":
		frame = rotate_bound( frame, 270 )
	return frame

# Frames are 1-based.
def getFrameNumber(n):
	n = n + 1
	return n

# -------------------------------

def give_focus_to_next_window_hack():
    # hack to switch to the cv window...
    cv2.namedWindow("GetFocus", cv2.WINDOW_NORMAL);
    #img = cv2.Mat.zeros(100, 100, cv2.8UC3);
    #cv2.imshow("GetFocus", img);
    cv2.imshow("GetFocus", 0);
    cv2.setWindowProperty("GetFocus", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN);
    cv2.waitKey(1);
    cv2.setWindowProperty("GetFocus", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_NORMAL);
    cv2.destroyWindow("GetFocus");
    # next window will get the focus!

# -------------------------------

# make the directory ... it must exist
mkdir_if_not_present( output_dir )


# read video file
print "loading video"
cap = cv2.VideoCapture(video_path)

give_focus_to_next_window_hack()

# count frame
count = 0
processed = 0
while(True):
	# Capture frame-by-frame
	ret, frame = cap.read()

	# if frame exists
	if ret == True:
		cv2.imshow("In", frame)
		cv2.moveWindow("In", 0,0)

		if nth_frame == 1 or count % nth_frame == 0:
			sys.stdout.write("count: %d   \r" % (count+1) )
			sys.stdout.flush()
			# Display results

			frame = rot_flip( frame, rot_flip_1 )
			frame = rot_flip( frame, rot_flip_2 )
			cv2.imshow("Out", frame)
			cv2.moveWindow("Out", 500,0)

			# save current frame
			output_path = ("%s/%s-%05d.jpg" % (output_dir, video_name, getFrameNumber(count)))
			#print output_path,"\r",
			ret = cv2.imwrite( output_path, frame )
			if not ret:
				print ("Error writing frame:", output_path)
				break
			processed = processed + 1

		# increment counter
		count = count + 1
	else:
		break

	# Give time for the frame to be displayed, otherwise it won't show
	key = cv2.waitKey(1) % 0x100
	if key == 27: # escape key
		break

print "\n",count," frames. ", processed, " processed"
# Clear all windows
cv2.destroyAllWindows()
