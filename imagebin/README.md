# Tools for Managing Images for Training

We have two ways of finding training images:

 - from search engines using search strings (**old way**)
 - get videos, extracting their frames, and labeling those (**new way**) 

We started with the first, and have accumulated about 1500 relevant images into 19 categories.  We've just now got working the second.

At this point the goal is to combine the results of each into a training set and start training from it.

Some tools that were built before can be modified for this.

The target directory is organized simply like this:

 -   `training_directory/category/images`

... categories with images.

The only issue is that old stuff and new stuff are starting to diverge as we evolve our understanding.  So now is the time to merge them and deal with the descrepancies. That is the training directory must be maintained & reviewed.

## Training Set Maintanance
The old way (using google search) resulted in output:

 - `images/...categories...`

That would be processed, aggregated, and copied to the target training directory using:

 - `image_cp --copy`

The `images` directory is **NOT archived in GIT** but instead to S3 using:

 - `cp_to_s3.sh` to copy it to s3, and
 - `cp_from_s3.sh` to copy it back.

I've also got two sets locally:

 - old imagebin/images # This is a save of the images source directory
 - ~/mldata # This is a save of the TRAINING DIRECTORY

And I've saved them in S3 `s3://peerwell-hazard-ml/images`

It's simple to merge them, but the new set has new categories. To use them the recognizer needs changes.

And it seems we should build a QA step using `localturk` to be able to review the training set.


## Searching & Organizing Images from Google
The goals here were to

 - make a tolerable manual process to get images to start training with
 - provide for some traceability of the images, for some are copyright and we'd like to know what is what and where they come from
 - give some flexibility to adjust the categories as we go
 
 With that said, this process is VERY manual.

### Getting images and categories

 1. Search & copy images from google

 ```
cd images # This is the directory to pull down and organize these images
# edit google-images-download.py for the search strings
# then run it:
python google-images-download.py
```
 This creates directories for each search containing the images found.
 
 Examine each search directory to see if the images are relevant and if so, move the search results directory into   the proper category.  So this is general formula:
 
 `images/category_name/search_result/images`

 1. Repeat above, until you have all the categories and a pile of search results for each.

### Reviewing Images
 1. Before you start reviewing images, there is a tool to get familiar with:
   -  `./image_cp.sh` to see help
 1. Use this tool to organize the categories and search results for manual labeling:
   -  `./image_cp.sh --setup # undestructive creation of directory structure`
   This will create subdirectories:
      -  `bad` to hold inappropriate images within each search
      -  `misc` to hold appropriate images you find from elsewhere 
 1. Use an viewing tool to look at each search result and move out the bad images into the `bad` subdirectories.
 1. Clean out bad images using `find_unspported_images.sh`

 1. Assess where you're at using `image_cp.sh`:
  - it prints of categories 
  - `./image_cp.sh --count # displays and counts number of images within each category and it's sub-search`
 
 1. When you have enough images, you can try and train from them...
 
### Copying images to a Training Directory
The images are now organized in searches underneath each category. To use them, they *good* images need to be aggregated up into the category. This is done in a copying process to a new directory that can be used directly for training.

```
./image_cp.sh --copy  # copies to training directory to be learned...
```

It will check if there's enough of each category and it will **NOT** copy them if there's too few. (This causes the training to **FAIL**).

You are good to go then.

--
--
# LATER
## For multi-object labeling

### Tools to Augment images to improve learning
 
 - https://github.com/mdbloice/Augmentor
 - https://github.com/aleju/imgaug
 - https://github.com/christopher5106/FastAnnotationTool
 - https://rectlabel.com/

