
Dependency installs:

	Run Python2:
		brew update
		- Update your path for brew usage in ~/.bash_profile:
		# For brew, make /usr/local/bin come ahead of /usr/bin so that brew installed programs are used
			export PATH="/usr/local/bin:$PATH"
		- close terminal and open a new one with the proper path and run:
		Try:
			brew install python2
			brew install python@2
	Test:
		which python
			should appear in /usr/bin/local
			if not link /usr/bin/local/python to where /usr/bin/local/python2
		pip  should exist
		- If not:
			brew uninstall python2
			brew install python2


	install virtualenv:
		pip install virtualenv
		vitualenv .
	
	Use virtualenv:

		source bin/activate

		And validate by:
			which python
				should appear deep in the local directory now

	install python stuff:

		pip install opencv-python
		pip install awscli
			#NB Installs aws cli using Python2 as per this virtualenv
		
			AWS CLI Install
				aws configure

	install other commandline tools:
		brew install csvkit
		nvm install 6.11.0
		npm install -g localturk
