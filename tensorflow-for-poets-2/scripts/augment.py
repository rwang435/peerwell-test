import Augmentor
import argparse

parser = argparse.ArgumentParser()
parser.add_argument(
        '--image_dir',
        type=str,
        #default='$HOME/ml_data/hazards_20180408',
        default='',
        help='Path to folders of labeled images.'
    )
parser.add_argument(
    '--output_dir',
    type=str,
    default='',
    help='Path to output folder of augmented images'
)
FLAGS, unparsed = parser.parse_known_args()

p = Augmentor.Pipeline(source_directory=FLAGS.image_dir, output_directory=FLAGS.output_dir)
# Point to a directory containing ground truth data.
# Images with the same file names will be added as ground truth data
# and augmented in parallel to the original data.

# Add operations to the pipeline as normal:
p.flip_left_right(probability=0.8)
p.flip_top_bottom(probability=0.2)
p.skew(probability=0.8)
p.random_distortion(probability=0.9, grid_width=9, grid_height=9, magnitude=3)
p.sample(15000)