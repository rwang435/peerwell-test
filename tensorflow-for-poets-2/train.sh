#!/bin/bash
#
# Train a model from images with sub-directories, and track via TensorBoard.
#
# Underlying Python script args.:
# - help if you want it
#			python -m scripts.retrain -h # help
# - To run new model:
#		python -m scripts.label_image --graph=${MAIN_DIR}/retrained_graph.pb --image=${MAIN_DIR}/flower_photos/roses/2414954629_3708a1a04d.jpg

# settings...
DATE=`date '+%Y-%m-%d-%H-%M'` # for naming things this run
MAIN_DIR='tf_files' # directory in which everything will be placed...

# ----------------------------
function usage() {
	echo "Usage"
	echo "  $0 training_directory"
	exit
}

if [ "$1" == "" ] ; then
	echo missing training set directory
	usage
fi

TRAINING_IMAGE_DIR=$1

echo
echo image dir: ${TRAINING_IMAGE_DIR}
# ----------------------------

# clear bottleneck directory just in case...
if [ "$2" == "clean" ] ; then
	echo cleaning bottlenecks
	rm -rf ${MAIN_DIR}/bottlenecks/ # need ending slash
else
	echo not cleaning bottlenecks
fi


# ----------------------------

# Start Tensorboard tracking of training progresss..
TENSORBOARD_OUTPUT="${MAIN_DIR}/training_summaries" # tensorboard info for this run dumped into here.

PIDS=`pgrep -f 'tensorboard'`
if [ "$PIDS" == "" ]
then
	echo starting tensorboard
	tensorboard --logdir "${TENSORBOARD_OUTPUT}" &
	sleep 5
else
	echo tensorboard already running
fi
echo
echo if it fails, try this:
echo '  pkill -f "tensorboard" # kill if running elsewhere...'
echo
echo tensorboard files are here: "${TENSORBOARD_OUTPUT}"
echo You can view tensorboard at this address:
echo '   http://0.0.0.0:6006/'
echo

# ----------------------------

# Startup training
# - params
IMAGE_SIZE=224
IMAGE_DIR="$HOME/ml_data/hazards_20180408/"
#ARCHITECTURE="mobilenet_0.50_${IMAGE_SIZE}"
ARCHITECTURE="mobilenet_v1_100_${IMAGE_SIZE}"
TFHUB="https://tfhub.dev/google/imagenet/${ARCHITECTURE}/feature_vector/1"
ML_STEPS=12000 # FYI: the built-in default is 4000
ML_LEARNING_RATE="0.01" # FYI: the built-in default is 0.01
BATCH_SIZE=100 # default 100
LAYER_CONFIG='relu_fc' # default 'fc' Other choices are: 'fc_fc, relu_fc', 'relu256_fc', 'relu256_relu128_fc', 'fc256_fc'


# Other training parameters
# --learning_rate=0.005 # default learning_rate is 0.01
# --how_many_training_steps has a default of 4,000 iterations.
# --image_dir is the location of the images where the subdirs are the category labels
# customs ones we added...
# --layer_add_config # Defaults to 'fc'. Other choices are: 'relu_fc', 'relu256_fc', 'relu256_relu128_fc', 'fc256_fc'
# USE These:
#   ML_STEPS=4000 # FYI: the built-in default is 4000
#   ML_LEARNING_RATE="0.01" # FYI: the built-in default is 0.01
#   BATCH_SIZE=100 # default 100
LAYER_ARGS="--layer_add_config=$LAYER_CONFIG"


# training image augmentation: These are SUPER slow
# --flip_left_right												# Whether to randomly flip half of the training images horizontally.
# --random_crop RANDOM_CROP								# A percentage determining how much of a margin to randomly crop off the training images.
# --random_scale RANDOM_SCALE							# A percentage determining how much to randomly scale up the size of the training images by.
# --random_brightness RANDOM_BRIGHTNESS		# A percentage determining how much to randomly multiply the training image input pixels up or down by.
#FLIP="--flip_left_right"


# Other defaulted params
# '--intermediate_store_frequency' defaults 0 # How many steps to store intermediate graph. If "0" then will not store.\
# '--testing_percentage', default 10 # What percentage of images to use as a test set.'
# '--validation_percentage', default 10 #='What percentage of images to use as a validation set.'
# '--eval_step_interval', default=10, help='How often to evaluate the training results.'
# '--train_batch_size', default=100, # help='How many images to train on at a time.'
# '--validation_batch_size', default=100, A value of -1 causes the entire validation set to be used, which leads to more stable results between training iterations
# '--print_misclassified_test_images', default=False, Whether to print out a list of all misclassified test images.\
# '--final_tensor_name', default='final_result', The name of the output classification layer in the retrained graph.\
BATCH_ARGS="--train_batch_size=$BATCH_SIZE"

# was
# --flip_left_right \
#	--how_many_training_steps=500 \

TRAINING_NAME="test" # used to identify this training run in tensorboard
RUN_ID="${ARCHITECTURE}_${TRAINING_NAME}_${DATE}_${ML_STEPS}_${BATCH_SIZE}_${ML_LEARNING_RATE}" # Id of this run visible in tensorboard
TB_RUN_DIR="${TENSORBOARD_OUTPUT}/${RUN_ID}"

# Our version supports adding differernt layers than juat fully connected one.
#python -m scripts.retrain \

echo ${TFHUB}
echo ${TRAINING_IMAGE_DIR}
echo ${MAIN_DIR}/bottlenecks

python -m scripts.ren_experimental \
		$FLIP $BATCH_ARGS $LAYER_ARGS \
	--image_dir=${IMAGE_DIR} \
    --print_misclassified_test_images \
		--how_many_training_steps=${ML_STEPS} \
		--learning_rate=${ML_LEARNING_RATE} \
	  --bottleneck_dir=${MAIN_DIR}/bottlenecks \
		--output_graph=${MAIN_DIR}/retrained_graph-${DATE}.pb \
		--output_labels=${MAIN_DIR}/retrained_labels-${DATE}.txt \
		--summaries_dir="${TB_RUN_DIR}" \
		--tfhub_module=${TFHUB}

echo exit code $? 
echo output: ${MAIN_DIR}/retrained_graph-${DATE}.pb ${MAIN_DIR}/retrained_labels-${DATE}.txt
echo To copy to the hazard app for use:
echo "cp tf_files/retrained_graph-${DATE}.pb tf_files/retrained_labels-${DATE}.txt  ../../peerwell-hazard-detection/hazards/"
echo $DATE
