#!/bin/bash
#
# Use the model to recognize an image
#

##IMAGE_PATH="tf_files/flower_photos/roses/2414954629_3708a1a04d.jpg"
#IMAGE_PATH="tf_files/hazards/good_stairway/29.jpg"
#IMAGE_PATH="test_images/test1.jpg"
## To run new model:
#python -m scripts.label_image --graph=tf_files/retrained_graph.pb --image="$IMAGE_PATH"

function usage() {
	echo "Usage"
	echo "  $0 validation_directory date_id"
	echo
	echo "  validation_directory is the locationg of validation set corresponding to the training set"
	echo "  date_id is a date format used to save the training model and labels in this form: Y-m-d-H-M"
	echo "     It can be seen in the last line of training output."
	echo
	exit
}
if [ "$1" == "" -o "$2" == "" ] ; then
	echo
	echo missing arguments
	echo
	usage
fi
#VALIDATION_DIR=~/ml_data/validate
VALIDATION_DIR=$1

#DATE_ID="2018-01-11-12-39"
#DATE_ID="2018-02-06-14-25"
#DATE_ID="2018-03-27-19-34"
#DATE_ID="2018-04-02-15-26" # 4000 epoch
#DATE_ID="2018-04-02-15-49" #8000 epoch
DATE_ID="$2"

MODEL_GRAPH_PATH=tf_files/retrained_graph-$DATE_ID.pb
MODEL_LABELS_PATH=tf_files/retrained_labels-$DATE_ID.txt


source ~/tensorflow/bin/activate


good=0
bad=0
list=$(find $VALIDATION_DIR -type f)
for i in $list ; do
	filename=$(basename $i)
	label=$(basename $(dirname $i))
	label_spaces=${label//_/ }

	echo recognizing $i
	result=$(python -m scripts.label_image --graph=${MODEL_GRAPH_PATH} --labels=${MODEL_LABELS_PATH} --image="$i")
	echo \"$result\" vs \"$label_spaces\"
	if [ "$result" = "$label_spaces" ] ; then
		good=$((good+1))
	else
		bad=$((bad+1))
	fi
	pct=$( echo "scale=2;100*$good/($bad+$good)" | bc -l )
	echo $good/$bad = ${pct}% correct

done
